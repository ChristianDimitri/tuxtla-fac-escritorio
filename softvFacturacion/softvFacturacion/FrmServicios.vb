Imports System.Data.SqlClient

Public Class FrmServicios

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Me.Close()
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click

        VALIDAAgregarServicioFacturacion(GloContrato, ComboBox1.SelectedValue)

        If eMsj.Length > 0 Then
            MessageBox.Show(eMsj)
            Exit Sub
        End If

        If Me.ComboBox1.SelectedValue = "CEXTV" Then
            GloClv_Txt = "CEXTV"
            'FrmExtecionesTv.Show()
            Dim FrmExt As New FrmExtecionesTv
            FrmExt.ShowDialog()

        ElseIf (Me.ComboBox1.SelectedValue = "CAMDO" Or Me.ComboBox1.SelectedValue = "CADIG" Or Me.ComboBox1.SelectedValue = "CANET") Then
            If SP_VALIDAACTIVOS(GloContrato) = 1 Then


                If Me.ComboBox1.SelectedValue = "CADIG" Then
                    GloClv_Txt = "CADIG"
                End If
                If Me.ComboBox1.SelectedValue = "CANET" Then
                    GloClv_Txt = "CANET"
                End If
                If Me.ComboBox1.SelectedValue = "CAMDO" Then
                    GloClv_Txt = "CAMDO"
                End If
                FormCAMDO.Show()
            Else
                MsgBox("Tiene que estar Activo para poder hacer un cambio de domicilio", MsgBoxStyle.Information, "Importante")

            End If

        ElseIf Me.ComboBox1.SelectedValue = "CEXTE" Then
            FrmCEXTETMP.Show()
        Else
            GloClv_Txt = Me.ComboBox1.SelectedValue
            GloBndExt = True
        End If
        Me.Close()
    End Sub

    Private Sub FrmServicios_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated

    End Sub

    Private Sub FrmServicios_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me)
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.DameServiciosFacturacionTableAdapter.Connection = CON
        Me.DameServiciosFacturacionTableAdapter.Fill(Me.NewsoftvDataSet.DameServiciosFacturacion, GloContrato)
        CON.Close()
    End Sub

    Private Sub VALIDAAgregarServicioFacturacion(ByVal Contrato As Integer, ByVal Clv_Txt As String)
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Contrato", SqlDbType.Int, Contrato)
        BaseII.CreateMyParameter("@Clv_Txt", SqlDbType.VarChar, Clv_Txt, 10)
        BaseII.CreateMyParameter("@Msj", ParameterDirection.Output, SqlDbType.VarChar, 150)
        BaseII.ProcedimientoOutPut("VALIDAAgregarServicioFacturacion")
        eMsj = ""
        eMsj = BaseII.dicoPar("@Msj").ToString
    End Sub

End Class