<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class BrwNotasdeCredito
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim ImporteLabel As System.Windows.Forms.Label
        Dim NOMBRELabel As System.Windows.Forms.Label
        Dim ClienteLabel As System.Windows.Forms.Label
        Dim FECHALabel As System.Windows.Forms.Label
        Dim Label4 As System.Windows.Forms.Label
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.SerieLabel = New System.Windows.Forms.Label()
        Me.Button10 = New System.Windows.Forms.Button()
        Me.Button9 = New System.Windows.Forms.Button()
        Me.Button8 = New System.Windows.Forms.Button()
        Me.Button6 = New System.Windows.Forms.Button()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Panel5 = New System.Windows.Forms.Panel()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Button4 = New System.Windows.Forms.Button()
        Me.CONTRATOTextBox = New System.Windows.Forms.TextBox()
        Me.NOMBRETextBox = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Button5 = New System.Windows.Forms.Button()
        Me.SplitContainer1 = New System.Windows.Forms.SplitContainer()
        Me.ComboBox1 = New System.Windows.Forms.ComboBox()
        Me.MUESTRASUCURSALES2BindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DataSetLydia = New softvFacturacion.DataSetLydia()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.BUSCANOTASDECREDITOBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DetalleNOTASDECREDITODataGridView = New System.Windows.Forms.DataGridView()
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DetalleNOTASDECREDITOBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.CMBNOMBRETextBox1 = New System.Windows.Forms.TextBox()
        Me.ImporteLabel1 = New System.Windows.Forms.Label()
        Me.ClienteLabel1 = New System.Windows.Forms.Label()
        Me.FECHALabel1 = New System.Windows.Forms.Label()
        Me.Clv_FacturaLabel1 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Button11 = New System.Windows.Forms.Button()
        Me.CMBLabel5 = New System.Windows.Forms.Label()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Button7 = New System.Windows.Forms.Button()
        Me.FECHATextBox = New System.Windows.Forms.TextBox()
        Me.Button12 = New System.Windows.Forms.Button()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.SERIETextBox = New System.Windows.Forms.TextBox()
        Me.CMBLabel1 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.DataGridView1 = New System.Windows.Forms.DataGridView()
        Me.Column1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column5 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column6 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column7 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column8 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column9 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column10 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column11 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column12 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column13 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column14 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MUESTRASUCURSALES2TableAdapter = New softvFacturacion.DataSetLydiaTableAdapters.MUESTRASUCURSALES2TableAdapter()
        Me.BUSCANOTASDECREDITOTableAdapter = New softvFacturacion.DataSetLydiaTableAdapters.BUSCANOTASDECREDITOTableAdapter()
        Me.DetalleNOTASDECREDITOTableAdapter = New softvFacturacion.DataSetLydiaTableAdapters.DetalleNOTASDECREDITOTableAdapter()
        Me.Cancela_NotaCreditoBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Cancela_NotaCreditoTableAdapter = New softvFacturacion.DataSetLydiaTableAdapters.Cancela_NotaCreditoTableAdapter()
        ImporteLabel = New System.Windows.Forms.Label()
        NOMBRELabel = New System.Windows.Forms.Label()
        ClienteLabel = New System.Windows.Forms.Label()
        FECHALabel = New System.Windows.Forms.Label()
        Label4 = New System.Windows.Forms.Label()
        Me.Panel5.SuspendLayout()
        Me.SplitContainer1.Panel1.SuspendLayout()
        Me.SplitContainer1.Panel2.SuspendLayout()
        Me.SplitContainer1.SuspendLayout()
        CType(Me.MUESTRASUCURSALES2BindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataSetLydia, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        CType(Me.BUSCANOTASDECREDITOBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DetalleNOTASDECREDITODataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DetalleNOTASDECREDITOBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Cancela_NotaCreditoBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ImporteLabel
        '
        ImporteLabel.AutoSize = True
        ImporteLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        ImporteLabel.ForeColor = System.Drawing.Color.White
        ImporteLabel.Location = New System.Drawing.Point(64, 110)
        ImporteLabel.Name = "ImporteLabel"
        ImporteLabel.Size = New System.Drawing.Size(55, 15)
        ImporteLabel.TabIndex = 35
        ImporteLabel.Text = "Monto :"
        '
        'NOMBRELabel
        '
        NOMBRELabel.AutoSize = True
        NOMBRELabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        NOMBRELabel.ForeColor = System.Drawing.Color.White
        NOMBRELabel.Location = New System.Drawing.Point(56, 82)
        NOMBRELabel.Name = "NOMBRELabel"
        NOMBRELabel.Size = New System.Drawing.Size(63, 15)
        NOMBRELabel.TabIndex = 34
        NOMBRELabel.Text = "Factura :"
        '
        'ClienteLabel
        '
        ClienteLabel.AutoSize = True
        ClienteLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        ClienteLabel.ForeColor = System.Drawing.Color.White
        ClienteLabel.Location = New System.Drawing.Point(50, 54)
        ClienteLabel.Name = "ClienteLabel"
        ClienteLabel.Size = New System.Drawing.Size(69, 15)
        ClienteLabel.TabIndex = 33
        ClienteLabel.Text = "Contrato :"
        '
        'FECHALabel
        '
        FECHALabel.AutoSize = True
        FECHALabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        FECHALabel.ForeColor = System.Drawing.Color.White
        FECHALabel.Location = New System.Drawing.Point(11, 159)
        FECHALabel.Name = "FECHALabel"
        FECHALabel.Size = New System.Drawing.Size(54, 15)
        FECHALabel.TabIndex = 32
        FECHALabel.Text = "Fecha :"
        '
        'Label4
        '
        Label4.AutoSize = True
        Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label4.ForeColor = System.Drawing.Color.White
        Label4.Location = New System.Drawing.Point(61, 136)
        Label4.Name = "Label4"
        Label4.Size = New System.Drawing.Size(52, 15)
        Label4.TabIndex = 101
        Label4.Text = "Saldo :"
        '
        'SerieLabel
        '
        Me.SerieLabel.AutoSize = True
        Me.SerieLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SerieLabel.ForeColor = System.Drawing.Color.White
        Me.SerieLabel.Location = New System.Drawing.Point(4, 29)
        Me.SerieLabel.Name = "SerieLabel"
        Me.SerieLabel.Size = New System.Drawing.Size(115, 15)
        Me.SerieLabel.TabIndex = 29
        Me.SerieLabel.Text = "Nota de Crédito :"
        '
        'Button10
        '
        Me.Button10.BackColor = System.Drawing.Color.DarkOrange
        Me.Button10.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button10.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button10.ForeColor = System.Drawing.Color.Black
        Me.Button10.Location = New System.Drawing.Point(866, 67)
        Me.Button10.Name = "Button10"
        Me.Button10.Size = New System.Drawing.Size(136, 36)
        Me.Button10.TabIndex = 46
        Me.Button10.Text = "&CONSULTA"
        Me.Button10.UseVisualStyleBackColor = False
        '
        'Button9
        '
        Me.Button9.BackColor = System.Drawing.Color.DarkOrange
        Me.Button9.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button9.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button9.ForeColor = System.Drawing.Color.Black
        Me.Button9.Location = New System.Drawing.Point(866, 116)
        Me.Button9.Name = "Button9"
        Me.Button9.Size = New System.Drawing.Size(136, 36)
        Me.Button9.TabIndex = 45
        Me.Button9.Text = "&MODIFICAR"
        Me.Button9.UseVisualStyleBackColor = False
        '
        'Button8
        '
        Me.Button8.BackColor = System.Drawing.Color.DarkOrange
        Me.Button8.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button8.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button8.ForeColor = System.Drawing.Color.Black
        Me.Button8.Location = New System.Drawing.Point(866, 19)
        Me.Button8.Name = "Button8"
        Me.Button8.Size = New System.Drawing.Size(136, 36)
        Me.Button8.TabIndex = 44
        Me.Button8.Text = "&NUEVO"
        Me.Button8.UseVisualStyleBackColor = False
        '
        'Button6
        '
        Me.Button6.BackColor = System.Drawing.Color.Orange
        Me.Button6.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button6.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button6.ForeColor = System.Drawing.Color.Black
        Me.Button6.Location = New System.Drawing.Point(866, 170)
        Me.Button6.Name = "Button6"
        Me.Button6.Size = New System.Drawing.Size(136, 83)
        Me.Button6.TabIndex = 38
        Me.Button6.Text = "&ReImprimir  Nota de Crédito"
        Me.Button6.UseVisualStyleBackColor = False
        '
        'Button2
        '
        Me.Button2.BackColor = System.Drawing.Color.Orange
        Me.Button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button2.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.ForeColor = System.Drawing.Color.Black
        Me.Button2.Location = New System.Drawing.Point(866, 268)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(136, 83)
        Me.Button2.TabIndex = 39
        Me.Button2.Text = "&Cancelar Nota de Crédito"
        Me.Button2.UseVisualStyleBackColor = False
        '
        'Panel5
        '
        Me.Panel5.Controls.Add(Me.Button3)
        Me.Panel5.Controls.Add(Me.Label6)
        Me.Panel5.Controls.Add(Me.Button4)
        Me.Panel5.Controls.Add(Me.CONTRATOTextBox)
        Me.Panel5.Controls.Add(Me.NOMBRETextBox)
        Me.Panel5.Controls.Add(Me.Label7)
        Me.Panel5.Location = New System.Drawing.Point(17, 244)
        Me.Panel5.Name = "Panel5"
        Me.Panel5.Size = New System.Drawing.Size(268, 169)
        Me.Panel5.TabIndex = 40
        '
        'Button3
        '
        Me.Button3.BackColor = System.Drawing.Color.DarkOrange
        Me.Button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button3.ForeColor = System.Drawing.Color.Black
        Me.Button3.Location = New System.Drawing.Point(14, 55)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(88, 23)
        Me.Button3.TabIndex = 8
        Me.Button3.Text = "&Buscar"
        Me.Button3.UseVisualStyleBackColor = False
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(11, 10)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(69, 15)
        Me.Label6.TabIndex = 21
        Me.Label6.Text = "Contrato :"
        '
        'Button4
        '
        Me.Button4.BackColor = System.Drawing.Color.DarkOrange
        Me.Button4.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button4.ForeColor = System.Drawing.Color.Black
        Me.Button4.Location = New System.Drawing.Point(14, 143)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(88, 23)
        Me.Button4.TabIndex = 10
        Me.Button4.Text = "&Buscar"
        Me.Button4.UseVisualStyleBackColor = False
        Me.Button4.Visible = False
        '
        'CONTRATOTextBox
        '
        Me.CONTRATOTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.CONTRATOTextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.CONTRATOTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CONTRATOTextBox.Location = New System.Drawing.Point(14, 28)
        Me.CONTRATOTextBox.Name = "CONTRATOTextBox"
        Me.CONTRATOTextBox.Size = New System.Drawing.Size(88, 21)
        Me.CONTRATOTextBox.TabIndex = 7
        '
        'NOMBRETextBox
        '
        Me.NOMBRETextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.NOMBRETextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.NOMBRETextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NOMBRETextBox.Location = New System.Drawing.Point(14, 116)
        Me.NOMBRETextBox.Name = "NOMBRETextBox"
        Me.NOMBRETextBox.Size = New System.Drawing.Size(249, 21)
        Me.NOMBRETextBox.TabIndex = 9
        Me.NOMBRETextBox.Visible = False
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(11, 98)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(66, 15)
        Me.Label7.TabIndex = 24
        Me.Label7.Text = "Nombre :"
        Me.Label7.Visible = False
        '
        'Button5
        '
        Me.Button5.BackColor = System.Drawing.Color.DarkOrange
        Me.Button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button5.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button5.ForeColor = System.Drawing.Color.Black
        Me.Button5.Location = New System.Drawing.Point(866, 692)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(136, 36)
        Me.Button5.TabIndex = 41
        Me.Button5.Text = "&SALIR"
        Me.Button5.UseVisualStyleBackColor = False
        '
        'SplitContainer1
        '
        Me.SplitContainer1.Location = New System.Drawing.Point(14, 12)
        Me.SplitContainer1.Name = "SplitContainer1"
        '
        'SplitContainer1.Panel1
        '
        Me.SplitContainer1.Panel1.AutoScroll = True
        Me.SplitContainer1.Panel1.BackColor = System.Drawing.Color.WhiteSmoke
        Me.SplitContainer1.Panel1.Controls.Add(Me.ComboBox1)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Label1)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Panel1)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Button11)
        Me.SplitContainer1.Panel1.Controls.Add(Me.CMBLabel5)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Button1)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Label5)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Button7)
        Me.SplitContainer1.Panel1.Controls.Add(Me.FECHATextBox)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Button12)
        Me.SplitContainer1.Panel1.Controls.Add(Me.TextBox1)
        Me.SplitContainer1.Panel1.Controls.Add(Me.SERIETextBox)
        Me.SplitContainer1.Panel1.Controls.Add(Me.CMBLabel1)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Label9)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Label3)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Label2)
        '
        'SplitContainer1.Panel2
        '
        Me.SplitContainer1.Panel2.Controls.Add(Me.DataGridView1)
        Me.SplitContainer1.Size = New System.Drawing.Size(836, 716)
        Me.SplitContainer1.SplitterDistance = 278
        Me.SplitContainer1.TabIndex = 42
        Me.SplitContainer1.TabStop = False
        '
        'ComboBox1
        '
        Me.ComboBox1.DataSource = Me.MUESTRASUCURSALES2BindingSource
        Me.ComboBox1.DisplayMember = "NOMBRE"
        Me.ComboBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBox1.FormattingEnabled = True
        Me.ComboBox1.Location = New System.Drawing.Point(19, 319)
        Me.ComboBox1.Name = "ComboBox1"
        Me.ComboBox1.Size = New System.Drawing.Size(209, 21)
        Me.ComboBox1.TabIndex = 25
        Me.ComboBox1.ValueMember = "CLV_SUCURSAL"
        '
        'MUESTRASUCURSALES2BindingSource
        '
        Me.MUESTRASUCURSALES2BindingSource.DataMember = "MUESTRASUCURSALES2"
        Me.MUESTRASUCURSALES2BindingSource.DataSource = Me.DataSetLydia
        '
        'DataSetLydia
        '
        Me.DataSetLydia.DataSetName = "DataSetLydia"
        Me.DataSetLydia.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Label1
        '
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(123, 162)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(127, 17)
        Me.Label1.TabIndex = 29
        Me.Label1.Text = "Ej. : dd/mm/aa"
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.DarkOrange
        Me.Panel1.Controls.Add(Label4)
        Me.Panel1.Controls.Add(Me.Label10)
        Me.Panel1.Controls.Add(Me.DetalleNOTASDECREDITODataGridView)
        Me.Panel1.Controls.Add(Me.CMBNOMBRETextBox1)
        Me.Panel1.Controls.Add(ImporteLabel)
        Me.Panel1.Controls.Add(Me.ImporteLabel1)
        Me.Panel1.Controls.Add(NOMBRELabel)
        Me.Panel1.Controls.Add(ClienteLabel)
        Me.Panel1.Controls.Add(Me.ClienteLabel1)
        Me.Panel1.Controls.Add(FECHALabel)
        Me.Panel1.Controls.Add(Me.FECHALabel1)
        Me.Panel1.Controls.Add(Me.SerieLabel)
        Me.Panel1.Controls.Add(Me.Clv_FacturaLabel1)
        Me.Panel1.Controls.Add(Me.Label8)
        Me.Panel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Panel1.Location = New System.Drawing.Point(3, 386)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(272, 312)
        Me.Panel1.TabIndex = 27
        '
        'Label10
        '
        Me.Label10.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BUSCANOTASDECREDITOBindingSource, "Saldo", True))
        Me.Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.ForeColor = System.Drawing.Color.White
        Me.Label10.Location = New System.Drawing.Point(137, 136)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(107, 23)
        Me.Label10.TabIndex = 102
        '
        'BUSCANOTASDECREDITOBindingSource
        '
        Me.BUSCANOTASDECREDITOBindingSource.DataMember = "BUSCANOTASDECREDITO"
        Me.BUSCANOTASDECREDITOBindingSource.DataSource = Me.DataSetLydia
        '
        'DetalleNOTASDECREDITODataGridView
        '
        Me.DetalleNOTASDECREDITODataGridView.AllowUserToAddRows = False
        Me.DetalleNOTASDECREDITODataGridView.AllowUserToDeleteRows = False
        Me.DetalleNOTASDECREDITODataGridView.AutoGenerateColumns = False
        Me.DetalleNOTASDECREDITODataGridView.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn3, Me.DataGridViewTextBoxColumn4, Me.DataGridViewTextBoxColumn2, Me.DataGridViewTextBoxColumn1})
        Me.DetalleNOTASDECREDITODataGridView.DataSource = Me.DetalleNOTASDECREDITOBindingSource
        Me.DetalleNOTASDECREDITODataGridView.Location = New System.Drawing.Point(0, 186)
        Me.DetalleNOTASDECREDITODataGridView.Name = "DetalleNOTASDECREDITODataGridView"
        Me.DetalleNOTASDECREDITODataGridView.ReadOnly = True
        Me.DetalleNOTASDECREDITODataGridView.Size = New System.Drawing.Size(273, 126)
        Me.DetalleNOTASDECREDITODataGridView.TabIndex = 47
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.DataPropertyName = "Column1"
        Me.DataGridViewTextBoxColumn3.HeaderText = "Factura"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.ReadOnly = True
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.DataPropertyName = "monto"
        Me.DataGridViewTextBoxColumn4.HeaderText = "Saldo"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        Me.DataGridViewTextBoxColumn4.ReadOnly = True
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.DataPropertyName = "FECHA_degeneracion"
        Me.DataGridViewTextBoxColumn2.HeaderText = "Fecha de Generación"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.ReadOnly = True
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.DataPropertyName = "clv_Notadecredito"
        Me.DataGridViewTextBoxColumn1.HeaderText = "Folio de Nota de Crédito"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        '
        'DetalleNOTASDECREDITOBindingSource
        '
        Me.DetalleNOTASDECREDITOBindingSource.DataMember = "DetalleNOTASDECREDITO"
        Me.DetalleNOTASDECREDITOBindingSource.DataSource = Me.DataSetLydia
        '
        'CMBNOMBRETextBox1
        '
        Me.CMBNOMBRETextBox1.BackColor = System.Drawing.Color.DarkOrange
        Me.CMBNOMBRETextBox1.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.CMBNOMBRETextBox1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BUSCANOTASDECREDITOBindingSource, "Column1", True))
        Me.CMBNOMBRETextBox1.ForeColor = System.Drawing.Color.White
        Me.CMBNOMBRETextBox1.Location = New System.Drawing.Point(140, 82)
        Me.CMBNOMBRETextBox1.Multiline = True
        Me.CMBNOMBRETextBox1.Name = "CMBNOMBRETextBox1"
        Me.CMBNOMBRETextBox1.ReadOnly = True
        Me.CMBNOMBRETextBox1.Size = New System.Drawing.Size(107, 23)
        Me.CMBNOMBRETextBox1.TabIndex = 100
        Me.CMBNOMBRETextBox1.TabStop = False
        '
        'ImporteLabel1
        '
        Me.ImporteLabel1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BUSCANOTASDECREDITOBindingSource, "monto", True))
        Me.ImporteLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ImporteLabel1.ForeColor = System.Drawing.Color.White
        Me.ImporteLabel1.Location = New System.Drawing.Point(140, 110)
        Me.ImporteLabel1.Name = "ImporteLabel1"
        Me.ImporteLabel1.Size = New System.Drawing.Size(107, 23)
        Me.ImporteLabel1.TabIndex = 36
        '
        'ClienteLabel1
        '
        Me.ClienteLabel1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BUSCANOTASDECREDITOBindingSource, "contrato", True))
        Me.ClienteLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ClienteLabel1.ForeColor = System.Drawing.Color.White
        Me.ClienteLabel1.Location = New System.Drawing.Point(140, 54)
        Me.ClienteLabel1.Name = "ClienteLabel1"
        Me.ClienteLabel1.Size = New System.Drawing.Size(107, 23)
        Me.ClienteLabel1.TabIndex = 34
        '
        'FECHALabel1
        '
        Me.FECHALabel1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BUSCANOTASDECREDITOBindingSource, "FECHA_degeneracion", True))
        Me.FECHALabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FECHALabel1.ForeColor = System.Drawing.Color.White
        Me.FECHALabel1.Location = New System.Drawing.Point(83, 159)
        Me.FECHALabel1.Name = "FECHALabel1"
        Me.FECHALabel1.Size = New System.Drawing.Size(164, 23)
        Me.FECHALabel1.TabIndex = 33
        '
        'Clv_FacturaLabel1
        '
        Me.Clv_FacturaLabel1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BUSCANOTASDECREDITOBindingSource, "clv_Notadecredito", True))
        Me.Clv_FacturaLabel1.ForeColor = System.Drawing.Color.DarkOrange
        Me.Clv_FacturaLabel1.Location = New System.Drawing.Point(140, 31)
        Me.Clv_FacturaLabel1.Name = "Clv_FacturaLabel1"
        Me.Clv_FacturaLabel1.Size = New System.Drawing.Size(107, 23)
        Me.Clv_FacturaLabel1.TabIndex = 0
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.ForeColor = System.Drawing.Color.White
        Me.Label8.Location = New System.Drawing.Point(4, 4)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(218, 18)
        Me.Label8.TabIndex = 0
        Me.Label8.Text = "Datos de la Nota de Crédito"
        '
        'Button11
        '
        Me.Button11.BackColor = System.Drawing.Color.DarkOrange
        Me.Button11.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button11.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button11.ForeColor = System.Drawing.Color.Black
        Me.Button11.Location = New System.Drawing.Point(20, 266)
        Me.Button11.Name = "Button11"
        Me.Button11.Size = New System.Drawing.Size(88, 23)
        Me.Button11.TabIndex = 8
        Me.Button11.Text = "&Buscar"
        Me.Button11.UseVisualStyleBackColor = False
        '
        'CMBLabel5
        '
        Me.CMBLabel5.AutoSize = True
        Me.CMBLabel5.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel5.ForeColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.CMBLabel5.Location = New System.Drawing.Point(13, 7)
        Me.CMBLabel5.Name = "CMBLabel5"
        Me.CMBLabel5.Size = New System.Drawing.Size(168, 24)
        Me.CMBLabel5.TabIndex = 0
        Me.CMBLabel5.Text = "Nota de Crédito  "
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.DarkOrange
        Me.Button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.ForeColor = System.Drawing.Color.Black
        Me.Button1.Location = New System.Drawing.Point(20, 188)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(88, 23)
        Me.Button1.TabIndex = 6
        Me.Button1.Text = "&Buscar"
        Me.Button1.UseVisualStyleBackColor = False
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(17, 221)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(69, 15)
        Me.Label5.TabIndex = 21
        Me.Label5.Text = "Contrato :"
        '
        'Button7
        '
        Me.Button7.BackColor = System.Drawing.Color.DarkOrange
        Me.Button7.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button7.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button7.ForeColor = System.Drawing.Color.Black
        Me.Button7.Location = New System.Drawing.Point(20, 109)
        Me.Button7.Name = "Button7"
        Me.Button7.Size = New System.Drawing.Size(88, 23)
        Me.Button7.TabIndex = 4
        Me.Button7.Text = "&Buscar"
        Me.Button7.UseVisualStyleBackColor = False
        '
        'FECHATextBox
        '
        Me.FECHATextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.FECHATextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.FECHATextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FECHATextBox.Location = New System.Drawing.Point(20, 161)
        Me.FECHATextBox.Name = "FECHATextBox"
        Me.FECHATextBox.Size = New System.Drawing.Size(88, 21)
        Me.FECHATextBox.TabIndex = 5
        '
        'Button12
        '
        Me.Button12.BackColor = System.Drawing.Color.DarkOrange
        Me.Button12.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button12.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button12.ForeColor = System.Drawing.Color.Black
        Me.Button12.Location = New System.Drawing.Point(20, 346)
        Me.Button12.Name = "Button12"
        Me.Button12.Size = New System.Drawing.Size(88, 23)
        Me.Button12.TabIndex = 10
        Me.Button12.Text = "&Buscar"
        Me.Button12.UseVisualStyleBackColor = False
        '
        'TextBox1
        '
        Me.TextBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBox1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TextBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox1.Location = New System.Drawing.Point(20, 239)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(88, 21)
        Me.TextBox1.TabIndex = 7
        '
        'SERIETextBox
        '
        Me.SERIETextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.SERIETextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SERIETextBox.Location = New System.Drawing.Point(20, 82)
        Me.SERIETextBox.Name = "SERIETextBox"
        Me.SERIETextBox.Size = New System.Drawing.Size(88, 21)
        Me.SERIETextBox.TabIndex = 2
        '
        'CMBLabel1
        '
        Me.CMBLabel1.AutoSize = True
        Me.CMBLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.CMBLabel1.Location = New System.Drawing.Point(12, 40)
        Me.CMBLabel1.Name = "CMBLabel1"
        Me.CMBLabel1.Size = New System.Drawing.Size(238, 20)
        Me.CMBLabel1.TabIndex = 0
        Me.CMBLabel1.Text = "Buscar Nota de Crédito Por :"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(17, 301)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(71, 15)
        Me.Label9.TabIndex = 24
        Me.Label9.Text = "Sucursal :"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(17, 64)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(115, 15)
        Me.Label3.TabIndex = 0
        Me.Label3.Text = "Nota de Crédito :"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(20, 143)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(54, 15)
        Me.Label2.TabIndex = 6
        Me.Label2.Text = "Fecha :"
        '
        'DataGridView1
        '
        Me.DataGridView1.AllowUserToAddRows = False
        Me.DataGridView1.AllowUserToDeleteRows = False
        Me.DataGridView1.AllowUserToOrderColumns = True
        Me.DataGridView1.BackgroundColor = System.Drawing.Color.WhiteSmoke
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.Chocolate
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DataGridView1.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Column1, Me.Column2, Me.Column3, Me.Column4, Me.Column5, Me.Column6, Me.Column7, Me.Column8, Me.Column9, Me.Column10, Me.Column11, Me.Column12, Me.Column13, Me.Column14})
        Me.DataGridView1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.DataGridView1.Location = New System.Drawing.Point(0, 0)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.ReadOnly = True
        Me.DataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.DataGridView1.Size = New System.Drawing.Size(554, 716)
        Me.DataGridView1.StandardTab = True
        Me.DataGridView1.TabIndex = 0
        Me.DataGridView1.TabStop = False
        '
        'Column1
        '
        Me.Column1.DataPropertyName = "clv_Notadecredito"
        Me.Column1.HeaderText = "Folio Nota de Credito (Real)"
        Me.Column1.Name = "Column1"
        Me.Column1.ReadOnly = True
        Me.Column1.Visible = False
        '
        'Column2
        '
        Me.Column2.DataPropertyName = "Nota_Credito"
        Me.Column2.HeaderText = "Folio Nota de Crédito"
        Me.Column2.Name = "Column2"
        Me.Column2.ReadOnly = True
        '
        'Column3
        '
        Me.Column3.DataPropertyName = "contrato"
        Me.Column3.HeaderText = "Contrato"
        Me.Column3.Name = "Column3"
        Me.Column3.ReadOnly = True
        '
        'Column4
        '
        Me.Column4.DataPropertyName = "FECHA_degeneracion"
        Me.Column4.HeaderText = "Fecha"
        Me.Column4.Name = "Column4"
        Me.Column4.ReadOnly = True
        '
        'Column5
        '
        Me.Column5.DataPropertyName = "monto"
        Me.Column5.HeaderText = "Monto"
        Me.Column5.Name = "Column5"
        Me.Column5.ReadOnly = True
        '
        'Column6
        '
        Me.Column6.DataPropertyName = "Saldo"
        Me.Column6.HeaderText = "Saldo"
        Me.Column6.Name = "Column6"
        Me.Column6.ReadOnly = True
        '
        'Column7
        '
        Me.Column7.DataPropertyName = "Status"
        Me.Column7.HeaderText = "Status"
        Me.Column7.Name = "Column7"
        Me.Column7.ReadOnly = True
        '
        'Column8
        '
        Me.Column8.DataPropertyName = "Column1"
        Me.Column8.HeaderText = "Factura"
        Me.Column8.Name = "Column8"
        Me.Column8.ReadOnly = True
        '
        'Column9
        '
        Me.Column9.DataPropertyName = "Usuario_captura"
        Me.Column9.HeaderText = "Usuario_captura"
        Me.Column9.Name = "Column9"
        Me.Column9.ReadOnly = True
        Me.Column9.Visible = False
        '
        'Column10
        '
        Me.Column10.DataPropertyName = "Usuario_autorizo"
        Me.Column10.HeaderText = "Usuario_autorizo"
        Me.Column10.Name = "Column10"
        Me.Column10.ReadOnly = True
        Me.Column10.Visible = False
        '
        'Column11
        '
        Me.Column11.DataPropertyName = "fecha_caducidad"
        Me.Column11.HeaderText = "fecha_caducidad"
        Me.Column11.Name = "Column11"
        Me.Column11.ReadOnly = True
        Me.Column11.Visible = False
        '
        'Column12
        '
        Me.Column12.DataPropertyName = "observaciones"
        Me.Column12.HeaderText = "observaciones"
        Me.Column12.Name = "Column12"
        Me.Column12.ReadOnly = True
        Me.Column12.Visible = False
        '
        'Column13
        '
        Me.Column13.DataPropertyName = "MotCan"
        Me.Column13.HeaderText = "MotCan"
        Me.Column13.Name = "Column13"
        Me.Column13.ReadOnly = True
        Me.Column13.Visible = False
        '
        'Column14
        '
        Me.Column14.DataPropertyName = "Clv_Factura_Aplicada"
        Me.Column14.HeaderText = "Clv_Factura_Aplicada"
        Me.Column14.Name = "Column14"
        Me.Column14.ReadOnly = True
        Me.Column14.Visible = False
        '
        'MUESTRASUCURSALES2TableAdapter
        '
        Me.MUESTRASUCURSALES2TableAdapter.ClearBeforeFill = True
        '
        'BUSCANOTASDECREDITOTableAdapter
        '
        Me.BUSCANOTASDECREDITOTableAdapter.ClearBeforeFill = True
        '
        'DetalleNOTASDECREDITOTableAdapter
        '
        Me.DetalleNOTASDECREDITOTableAdapter.ClearBeforeFill = True
        '
        'Cancela_NotaCreditoBindingSource
        '
        Me.Cancela_NotaCreditoBindingSource.DataMember = "Cancela_NotaCredito"
        Me.Cancela_NotaCreditoBindingSource.DataSource = Me.DataSetLydia
        '
        'Cancela_NotaCreditoTableAdapter
        '
        Me.Cancela_NotaCreditoTableAdapter.ClearBeforeFill = True
        '
        'BrwNotasdeCredito
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1016, 741)
        Me.Controls.Add(Me.SplitContainer1)
        Me.Controls.Add(Me.Button10)
        Me.Controls.Add(Me.Button9)
        Me.Controls.Add(Me.Button8)
        Me.Controls.Add(Me.Button6)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.Panel5)
        Me.Controls.Add(Me.Button5)
        Me.MaximizeBox = False
        Me.Name = "BrwNotasdeCredito"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Catálogo Notas de Crédito"
        Me.Panel5.ResumeLayout(False)
        Me.Panel5.PerformLayout()
        Me.SplitContainer1.Panel1.ResumeLayout(False)
        Me.SplitContainer1.Panel1.PerformLayout()
        Me.SplitContainer1.Panel2.ResumeLayout(False)
        Me.SplitContainer1.ResumeLayout(False)
        CType(Me.MUESTRASUCURSALES2BindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataSetLydia, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.BUSCANOTASDECREDITOBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DetalleNOTASDECREDITODataGridView, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DetalleNOTASDECREDITOBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Cancela_NotaCreditoBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Button10 As System.Windows.Forms.Button
    Friend WithEvents Button9 As System.Windows.Forms.Button
    Friend WithEvents Button8 As System.Windows.Forms.Button
    Friend WithEvents Button6 As System.Windows.Forms.Button
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Panel5 As System.Windows.Forms.Panel
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Button4 As System.Windows.Forms.Button
    Friend WithEvents CONTRATOTextBox As System.Windows.Forms.TextBox
    Friend WithEvents NOMBRETextBox As System.Windows.Forms.TextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents SplitContainer1 As System.Windows.Forms.SplitContainer
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents CMBNOMBRETextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents ImporteLabel1 As System.Windows.Forms.Label
    Friend WithEvents ClienteLabel1 As System.Windows.Forms.Label
    Friend WithEvents FECHALabel1 As System.Windows.Forms.Label
    Friend WithEvents Clv_FacturaLabel1 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents CMBLabel5 As System.Windows.Forms.Label
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Button7 As System.Windows.Forms.Button
    Friend WithEvents FECHATextBox As System.Windows.Forms.TextBox
    Friend WithEvents SERIETextBox As System.Windows.Forms.TextBox
    Friend WithEvents CMBLabel1 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents DataGridView1 As System.Windows.Forms.DataGridView
    Friend WithEvents Button11 As System.Windows.Forms.Button
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Button12 As System.Windows.Forms.Button
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents DataSetLydia As softvFacturacion.DataSetLydia
    Friend WithEvents FacturaDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents MUESTRASUCURSALES2BindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MUESTRASUCURSALES2TableAdapter As softvFacturacion.DataSetLydiaTableAdapters.MUESTRASUCURSALES2TableAdapter
    Friend WithEvents ComboBox1 As System.Windows.Forms.ComboBox
    Friend WithEvents BUSCANOTASDECREDITOBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents BUSCANOTASDECREDITOTableAdapter As softvFacturacion.DataSetLydiaTableAdapters.BUSCANOTASDECREDITOTableAdapter
    Friend WithEvents DetalleNOTASDECREDITOBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DetalleNOTASDECREDITOTableAdapter As softvFacturacion.DataSetLydiaTableAdapters.DetalleNOTASDECREDITOTableAdapter
    Friend WithEvents DetalleNOTASDECREDITODataGridView As System.Windows.Forms.DataGridView
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Cancela_NotaCreditoBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Cancela_NotaCreditoTableAdapter As softvFacturacion.DataSetLydiaTableAdapters.Cancela_NotaCreditoTableAdapter
    Friend WithEvents SerieLabel As System.Windows.Forms.Label
    Friend WithEvents Column1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column6 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column7 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column8 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column9 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column10 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column11 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column12 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column13 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column14 As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
