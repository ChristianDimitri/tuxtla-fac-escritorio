﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmConvenioCobroMaterial
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.gbDescConvenio = New System.Windows.Forms.GroupBox()
        Me.txtImporteMensual = New System.Windows.Forms.TextBox()
        Me.txtNumPagos = New System.Windows.Forms.TextBox()
        Me.txtImporte = New System.Windows.Forms.TextBox()
        Me.lblContrato = New System.Windows.Forms.Label()
        Me.txtContrato = New System.Windows.Forms.TextBox()
        Me.lblImporte = New System.Windows.Forms.Label()
        Me.lblNumPagos = New System.Windows.Forms.Label()
        Me.lblImporteMensual = New System.Windows.Forms.Label()
        Me.btnAceptarConvenio = New System.Windows.Forms.Button()
        Me.btnRechazarConvenio = New System.Windows.Forms.Button()
        Me.gbDescConvenio.SuspendLayout()
        Me.SuspendLayout()
        '
        'gbDescConvenio
        '
        Me.gbDescConvenio.Controls.Add(Me.txtImporteMensual)
        Me.gbDescConvenio.Controls.Add(Me.txtNumPagos)
        Me.gbDescConvenio.Controls.Add(Me.txtImporte)
        Me.gbDescConvenio.Controls.Add(Me.lblContrato)
        Me.gbDescConvenio.Controls.Add(Me.txtContrato)
        Me.gbDescConvenio.Controls.Add(Me.lblImporte)
        Me.gbDescConvenio.Controls.Add(Me.lblNumPagos)
        Me.gbDescConvenio.Controls.Add(Me.lblImporteMensual)
        Me.gbDescConvenio.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbDescConvenio.Location = New System.Drawing.Point(12, 12)
        Me.gbDescConvenio.Name = "gbDescConvenio"
        Me.gbDescConvenio.Size = New System.Drawing.Size(347, 178)
        Me.gbDescConvenio.TabIndex = 13
        Me.gbDescConvenio.TabStop = False
        Me.gbDescConvenio.Text = "Desglose Convenio"
        '
        'txtImporteMensual
        '
        Me.txtImporteMensual.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtImporteMensual.Location = New System.Drawing.Point(174, 135)
        Me.txtImporteMensual.Name = "txtImporteMensual"
        Me.txtImporteMensual.ReadOnly = True
        Me.txtImporteMensual.Size = New System.Drawing.Size(162, 24)
        Me.txtImporteMensual.TabIndex = 11
        '
        'txtNumPagos
        '
        Me.txtNumPagos.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNumPagos.Location = New System.Drawing.Point(174, 99)
        Me.txtNumPagos.Name = "txtNumPagos"
        Me.txtNumPagos.ReadOnly = True
        Me.txtNumPagos.Size = New System.Drawing.Size(162, 24)
        Me.txtNumPagos.TabIndex = 10
        '
        'txtImporte
        '
        Me.txtImporte.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtImporte.Location = New System.Drawing.Point(174, 62)
        Me.txtImporte.Name = "txtImporte"
        Me.txtImporte.ReadOnly = True
        Me.txtImporte.Size = New System.Drawing.Size(162, 24)
        Me.txtImporte.TabIndex = 9
        '
        'lblContrato
        '
        Me.lblContrato.AutoSize = True
        Me.lblContrato.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblContrato.Location = New System.Drawing.Point(68, 27)
        Me.lblContrato.Name = "lblContrato"
        Me.lblContrato.Size = New System.Drawing.Size(89, 20)
        Me.lblContrato.TabIndex = 3
        Me.lblContrato.Text = "Contrato :"
        '
        'txtContrato
        '
        Me.txtContrato.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtContrato.Location = New System.Drawing.Point(174, 27)
        Me.txtContrato.Name = "txtContrato"
        Me.txtContrato.ReadOnly = True
        Me.txtContrato.Size = New System.Drawing.Size(162, 24)
        Me.txtContrato.TabIndex = 8
        '
        'lblImporte
        '
        Me.lblImporte.AutoSize = True
        Me.lblImporte.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblImporte.Location = New System.Drawing.Point(31, 62)
        Me.lblImporte.Name = "lblImporte"
        Me.lblImporte.Size = New System.Drawing.Size(126, 20)
        Me.lblImporte.TabIndex = 4
        Me.lblImporte.Text = "Importe Total :"
        '
        'lblNumPagos
        '
        Me.lblNumPagos.AutoSize = True
        Me.lblNumPagos.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNumPagos.Location = New System.Drawing.Point(56, 99)
        Me.lblNumPagos.Name = "lblNumPagos"
        Me.lblNumPagos.Size = New System.Drawing.Size(101, 20)
        Me.lblNumPagos.TabIndex = 5
        Me.lblNumPagos.Text = "No. Pagos :"
        '
        'lblImporteMensual
        '
        Me.lblImporteMensual.AutoSize = True
        Me.lblImporteMensual.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblImporteMensual.Location = New System.Drawing.Point(7, 135)
        Me.lblImporteMensual.Name = "lblImporteMensual"
        Me.lblImporteMensual.Size = New System.Drawing.Size(150, 20)
        Me.lblImporteMensual.TabIndex = 6
        Me.lblImporteMensual.Text = "Importe por Mes :"
        '
        'btnAceptarConvenio
        '
        Me.btnAceptarConvenio.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAceptarConvenio.Location = New System.Drawing.Point(38, 197)
        Me.btnAceptarConvenio.Name = "btnAceptarConvenio"
        Me.btnAceptarConvenio.Size = New System.Drawing.Size(106, 30)
        Me.btnAceptarConvenio.TabIndex = 14
        Me.btnAceptarConvenio.Text = "&Aceptar"
        Me.btnAceptarConvenio.UseVisualStyleBackColor = True
        '
        'btnRechazarConvenio
        '
        Me.btnRechazarConvenio.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnRechazarConvenio.Location = New System.Drawing.Point(211, 197)
        Me.btnRechazarConvenio.Name = "btnRechazarConvenio"
        Me.btnRechazarConvenio.Size = New System.Drawing.Size(106, 30)
        Me.btnRechazarConvenio.TabIndex = 15
        Me.btnRechazarConvenio.Text = "&Rechazar"
        Me.btnRechazarConvenio.UseVisualStyleBackColor = True
        '
        'FrmConvenioCobroMaterial
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(375, 239)
        Me.Controls.Add(Me.btnAceptarConvenio)
        Me.Controls.Add(Me.btnRechazarConvenio)
        Me.Controls.Add(Me.gbDescConvenio)
        Me.Name = "FrmConvenioCobroMaterial"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "FrmConvenioCobroMaterial"
        Me.gbDescConvenio.ResumeLayout(False)
        Me.gbDescConvenio.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents gbDescConvenio As System.Windows.Forms.GroupBox
    Friend WithEvents txtImporteMensual As System.Windows.Forms.TextBox
    Friend WithEvents txtNumPagos As System.Windows.Forms.TextBox
    Friend WithEvents txtImporte As System.Windows.Forms.TextBox
    Friend WithEvents lblContrato As System.Windows.Forms.Label
    Friend WithEvents txtContrato As System.Windows.Forms.TextBox
    Friend WithEvents lblImporte As System.Windows.Forms.Label
    Friend WithEvents lblNumPagos As System.Windows.Forms.Label
    Friend WithEvents lblImporteMensual As System.Windows.Forms.Label
    Friend WithEvents btnAceptarConvenio As System.Windows.Forms.Button
    Friend WithEvents btnRechazarConvenio As System.Windows.Forms.Button
End Class
