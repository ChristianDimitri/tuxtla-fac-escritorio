﻿Public Class FrmGuardaArqueos

    Public CajeraArqueo As String
    Public FechaArqueo As Date
    Public IdCorte As Long = 0

    Private Sub FrmGuardaArqueos_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me)

        '''''OBTENEMOS LOS DATOS TOTALES DEL DÍA (INICIO)
        If IdCorte > 0 Then
            ConsultaCortesGlobales(IdCorte, "T", "TODOS", "TODOS")
            Me.btnAceptar.Enabled = False
        Else
            uspCalculaCortesGlobales(CajeraArqueo, FechaArqueo, GloUsuario)
            Me.btnAceptar.Enabled = True
        End If
        '''''OBTENEMOS LOS DATOS TOTALES DEL DÍA (FIN)
    End Sub

    Private Sub uspCalculaCortesGlobales(ByVal prmCajera As String, ByVal prmFecha As Date, ByVal prmAutoriza As String)
        '''''EJECUTAMOS EL PROCEDIMIENTO QUE NOS VA A DAR LOS TOTALES DE LOS COBRADO EN EL DÍA POR LA CAJERA (INICIO)
        Dim DT As New DataTable

        ControlEfectivoClass.limpiaParametros()

        ControlEfectivoClass.CreateMyParameter("@cajera", SqlDbType.VarChar, prmCajera, 5)
        ControlEfectivoClass.CreateMyParameter("@fecha", SqlDbType.DateTime, prmFecha)
        ControlEfectivoClass.CreateMyParameter("@autoriza", SqlDbType.VarChar, prmAutoriza, 5)

        DT = ControlEfectivoClass.ConsultaDT("uspCalculaCortesGlobales")
        Me.txtCajera.Text = CajeraArqueo
        Me.dtpFechaCorte.Text = FechaArqueo
        Me.dtpFechaGeneracion.Text = Today
        Me.txtTotalEfectivo.Text = CDec(DT.Rows(0)("totalEfectivo")).ToString("#.00")
        Me.txtTotalTarjeta.Text = CDec(DT.Rows(0)("totalTarjeta")).ToString("#.00")
        Me.txtTotalCheques.Text = CDec(DT.Rows(0)("totalCheques")).ToString("#.00")
        Me.txtEntregasParciales.Text = CDec(DT.Rows(0)("entregasParciales")).ToString("#.00")
        Me.txtTotalGastos.Text = CDec(DT.Rows(0)("gastos")).ToString("#.00")
        Me.txtAutoriza.Text = prmAutoriza
        Me.txtTotalCobrado.Text = CDec(DT.Rows(0)("totalCobrado")).ToString("#.00")
        '''''EJECUTAMOS EL PROCEDIMIENTO QUE NOS VA A DAR LOS TOTALES DE LOS COBRADO EN EL DÍA POR LA CAJERA (FIN)
    End Sub

    Private Sub ConsultaCortesGlobales(ByVal prmIdCorte As Long, ByVal prmStatus As String, ByVal prmCajera As String, ByVal prmAutoriza As String)
        '''''EJECUTAMOS EL PROCEDIMIENTO QUE NOS VA A DAR LOS TOTALES DE LOS COBRADO EN EL DÍA POR LA CAJERA (INICIO)
        Dim DT As New DataTable

        ControlEfectivoClass.limpiaParametros()

        ControlEfectivoClass.CreateMyParameter("@idCorte", SqlDbType.BigInt, prmIdCorte)
        ControlEfectivoClass.CreateMyParameter("@status", SqlDbType.VarChar, prmStatus, 1)
        ControlEfectivoClass.CreateMyParameter("@cajera", SqlDbType.VarChar, prmCajera, 5)
        ControlEfectivoClass.CreateMyParameter("@autoriza", SqlDbType.VarChar, prmAutoriza, 5)

        DT = ControlEfectivoClass.ConsultaDT("uspConsultaCortesGlobales")
        Me.txtCajera.Text = DT.Rows(0)("cajera").ToString
        Me.dtpFechaCorte.Text = DT.Rows(0)("fechaCorte").ToString
        Me.dtpFechaGeneracion.Text = DT.Rows(0)("fechaGeneracion").ToString
        Me.txtTotalEfectivo.Text = CDec(DT.Rows(0)("totalEfectivo")).ToString("#.00")
        Me.txtTotalTarjeta.Text = CDec(DT.Rows(0)("totalTarjeta")).ToString("#.00")
        Me.txtTotalCheques.Text = CDec(DT.Rows(0)("totalCheques")).ToString("#.00")
        Me.txtEntregasParciales.Text = CDec(DT.Rows(0)("entregasParciales")).ToString("#.00")
        Me.txtTotalGastos.Text = CDec(DT.Rows(0)("gastos")).ToString("#.00")
        Me.txtAutoriza.Text = DT.Rows(0)("autoriza").ToString
        Me.txtTotalCobrado.Text = CDec(DT.Rows(0)("totalCobrado")).ToString("#.00")
        '''''EJECUTAMOS EL PROCEDIMIENTO QUE NOS VA A DAR LOS TOTALES DE LOS COBRADO EN EL DÍA POR LA CAJERA (FIN)
    End Sub

    Private Sub uspInsertaCortesGlobales(ByVal prmCajera As String, ByVal prmFechaCorte As Date, ByVal prmTotalEfectivo As Decimal, ByVal prmTotalTarjeta As Decimal, _
                                         ByVal prmTotalCheques As Decimal, ByVal prmEntregasParciales As Decimal, ByVal prmTotalCobrado As Decimal, ByVal prmGastos As Decimal, _
                                         ByVal prmAutoriza As String)
        '''''EJECUTAMOS ELPROCEDIMIENTO QUE VA A HACER LA INSERCIÓN DE LOS DATOS EN LA TABLA DE LOS ARQUEOS (INICIO)
        ControlEfectivoClass.limpiaParametros()

        ControlEfectivoClass.CreateMyParameter("@cajera", SqlDbType.VarChar, prmCajera, 5)
        ControlEfectivoClass.CreateMyParameter("@fechaCorte", SqlDbType.DateTime, prmFechaCorte)
        ControlEfectivoClass.CreateMyParameter("@totalEfectivo", SqlDbType.Money, prmTotalEfectivo)
        ControlEfectivoClass.CreateMyParameter("@totalTarjeta", SqlDbType.Money, prmTotalTarjeta)
        ControlEfectivoClass.CreateMyParameter("@totalCheques", SqlDbType.Money, prmTotalCheques)
        ControlEfectivoClass.CreateMyParameter("@entregasParciales", SqlDbType.Money, prmEntregasParciales)
        ControlEfectivoClass.CreateMyParameter("@totalCobrado", SqlDbType.Money, prmTotalCobrado)
        ControlEfectivoClass.CreateMyParameter("@gastos", SqlDbType.Money, prmGastos)
        ControlEfectivoClass.CreateMyParameter("@autoriza", SqlDbType.VarChar, prmAutoriza, 5)

        If ControlEfectivoClass.InsertaBol("uspInsertaCortesGlobales") = False Then
            MsgBox("Ya Existe un Arqueo Almacenado del Día: " + prmFechaCorte, MsgBoxStyle.Information)
            Exit Sub
        Else
            MsgBox("Almacenado Exitosamente", MsgBoxStyle.Information)
            Me.Close()
        End If
        '''''EJECUTAMOS ELPROCEDIMIENTO QUE VA A HACER LA INSERCIÓN DE LOS DATOS EN LA TABLA DE LOS ARQUEOS (INICIO)
    End Sub

    Private Sub btnAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAceptar.Click
        '''''SI AUTORIZAN EL ARQUEO PROCEDEMOS A ALMACENARLO (INICIO)
        uspInsertaCortesGlobales(Me.txtCajera.Text, Me.dtpFechaCorte.Value, Me.txtTotalEfectivo.Text, Me.txtTotalTarjeta.Text, Me.txtTotalCheques.Text, Me.txtEntregasParciales.Text, _
                                 Me.txtTotalCobrado.Text, Me.txtTotalGastos.Text, GloUsuario)
        '''''SI AUTORIZAN EL ARQUEO PROCEDEMOS A ALMACENARLO (FIN)
    End Sub

    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        Me.Close()
    End Sub
End Class

