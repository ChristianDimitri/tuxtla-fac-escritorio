Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports System.Data.SqlClient

Public Class FrmFACLogitel
    Private customersByCityReport As ReportDocument
    Dim BndError As Integer = 0
    Dim Loc_Clv_Vendedor As Integer = 0
    Dim Loc_Folio As Long = 0
    Dim Loc_Serie As String = Nothing
    Dim Msg As String = Nothing
    Dim bloqueado, identi As Integer
    Dim Bnd As Integer = 0
    Dim CuantasTv As Integer = 0
    Private LocNomImpresora_Contratos As String = Nothing
    Private LocNomImpresora_Tarjetas As String = Nothing
    Dim SiPagos As Integer = 0
    Private eMsjTickets As String = Nothing
    Private eActTickets As Boolean = False
    Private eCont As Integer = 1
    Private eRes As Integer = 0
    Private ePideAparato As Integer = 0
    Private eClv_Detalle As Long = 0
    Private Checa_recurrente As Integer = Nothing
    Private es_recurrente As Boolean = False
    Private ya_entre As Integer = 0
    Private Sub Guarda_Cobro_Rel_Supervisor_cobrosRec(ByVal clv_factura As Long, ByVal clv_supervisor As String, ByVal contrato As Long)
        Dim CON90 As New SqlConnection(MiConexion)
        Dim CMD As New SqlCommand()
        Try
            CMD = New SqlCommand()
            CON90.Open()
            With CMD
                .CommandText = "Guarda_Cobro_Rel_Supervisor_cobrosRec"
                .CommandType = CommandType.StoredProcedure
                .Connection = CON90
                .CommandTimeout = 0

                Dim prm As New SqlParameter("@clv_factura", SqlDbType.BigInt)
                prm.Direction = ParameterDirection.Input
                prm.Value = clv_factura
                .Parameters.Add(prm)

                Dim prm1 As New SqlParameter("@clv_supervisor", SqlDbType.VarChar, 5)
                prm1.Direction = ParameterDirection.Input
                prm1.Value = clv_supervisor
                .Parameters.Add(prm1)

                Dim prm2 As New SqlParameter("@contrato", SqlDbType.BigInt)
                prm2.Direction = ParameterDirection.Input
                prm2.Value = contrato
                .Parameters.Add(prm2)
                Dim i As Integer = .ExecuteNonQuery()
            End With
            CON90.Close()
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub Checa_es_recurrente()
        Dim CON80 As New SqlConnection(MiConexion)
        Dim CMD As New SqlCommand()
        Try
            CMD = New SqlCommand()
            CON80.Open()
            With CMD
                .CommandText = "Checa_si_es_pago_cargo_recurrente"
                .CommandType = CommandType.StoredProcedure
                .Connection = CON80
                .CommandTimeout = 0

                Dim prm As New SqlParameter("@contrato", SqlDbType.BigInt)
                prm.Direction = ParameterDirection.Input
                prm.Value = CLng(Me.ContratoTextBox.Text)
                .Parameters.Add(prm)

                Dim prm1 As New SqlParameter("@error", SqlDbType.Int)
                prm1.Direction = ParameterDirection.Output
                prm1.Value = 0
                .Parameters.Add(prm1)

                Dim i As Integer = .ExecuteNonQuery()
                Checa_recurrente = prm1.Value
            End With
            CON80.Close()
            If Checa_recurrente = 1 Then
                locband_pant = 8
                es_recurrente = True
                If eAccesoAdmin = False Then
                    Bloque(False)
                    FrmSupervisor.Show()
                Else
                    es_recurrente = False
                    Bloque(True)
                End If
                bndGraboClienteRecurrente = True
            End If
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub




    Private Sub SetDBLogonForReport(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument)
        customersByCityReport.DataSourceConnections(0).SetConnection(GloServerName, GloDatabaseName, GloUserID, GloPassword)
        'customersByCityReport.SetDatabaseLogon(GloUserID, GloPassword, GloServerName, GloDatabaseName)

        Dim myTables As Tables = myReportDocument.Database.Tables
        Dim myTable As CrystalDecisions.CrystalReports.Engine.Table
        For Each myTable In myTables
            Dim myTableLogonInfo As TableLogOnInfo = myTable.LogOnInfo
            myTableLogonInfo.ConnectionInfo = myConnectionInfo
            myTable.ApplyLogOnInfo(myTableLogonInfo)
            myTable.Location = GloDatabaseName & ".dbo." & myTable.Location.Substring(myTable.Location.LastIndexOf(".") + 1)
        Next
    End Sub

    Private Sub SetDBLogonForSubReport(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument)
        customersByCityReport.Subreports(0).SetDatabaseLogon(GloUserID, GloPassword, GloServerName, GloDatabaseName)
        'customersByCityReport.Subreports(0).DataSourceConnections(0).SetConnection(GloServerName, GloDatabaseName, GloUserID, GloPassword)

        Dim I As Integer = myReportDocument.Subreports.Count
        Dim X As Integer = 0
        For X = 0 To I - 1
            Dim myTables As Tables = myReportDocument.Subreports(X).Database.Tables
            Dim myTable As CrystalDecisions.CrystalReports.Engine.Table
            For Each myTable In myTables
                Dim myTableLogonInfo As TableLogOnInfo = myTable.LogOnInfo
                myTableLogonInfo.ConnectionInfo = myConnectionInfo
                myTable.ApplyLogOnInfo(myTableLogonInfo)
                myTable.Location = GloDatabaseName & ".dbo." & myTable.Location.Substring(myTable.Location.LastIndexOf(".") + 1)
            Next
        Next X
    End Sub

    'Private Sub SetDBLogonForReport2(ByVal myConnectionInfo As ConnectionInfo)
    ' Dim myTableLogOnInfos As TableLogOnInfos = Me.CrystalReportViewer1.LogOnInfo
    '     For Each myTableLogOnInfo As TableLogOnInfo In myTableLogOnInfos
    '         myTableLogOnInfo.ConnectionInfo = myConnectionInfo
    '     Next
    ' End Sub

    Private Sub ConfigureCrystalReports(ByVal Clv_Factura As Long)
        'ConfigureCrystalReports
        Try
            Dim ba As Boolean = False
            customersByCityReport = New ReportDocument
            Dim connectionInfo As New ConnectionInfo
            '"Data Source=SERVER-2003\SQLEXPRESS;Initial Catalog=Newsoftv;Persist Security Info"& _ 
            '    "=True;User ID=DeSistema;Password=1975huli")
            connectionInfo.ServerName = GloServerName
            connectionInfo.DatabaseName = GloDatabaseName
            connectionInfo.UserID = GloUserID
            connectionInfo.Password = GloPassword

            Dim reportPath As String = Nothing

            '        If GloImprimeTickets = False Then
            'reportPath = Application.StartupPath + "\Reportes\" + "ReporteCajas.rpt"
            'Else
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            Me.BusFacFiscalTableAdapter.Connection = CON
            Me.BusFacFiscalTableAdapter.Fill(Me.NewsoftvDataSet2.BusFacFiscal, Clv_Factura, identi)
            CON.Dispose()
            CON.Close()

            eActTickets = False
            Dim CON2 As New SqlConnection(MiConexion)
            CON2.Open()
            Me.DameGeneralMsjTicketsTableAdapter.Connection = CON2
            Me.DameGeneralMsjTicketsTableAdapter.Fill(Me.EricDataSet2.DameGeneralMsjTickets, eMsjTickets, eActTickets)
            CON2.Close()

            If IdSistema = "SA" And facnormal = True And identi > 0 Then
                reportPath = RutaReportes + "\ReporteCajasTvRey.rpt"
                ba = True
            ElseIf IdSistema = "TO" And facnormal = True And identi > 0 Then
                reportPath = RutaReportes + "\ReporteCajasCabSta.rpt"
                ba = True
            ElseIf IdSistema = "AG" And facnormal = True And identi > 0 Then
                reportPath = RutaReportes + "\ReporteCajasGiga.rpt"
                ba = True
            ElseIf IdSistema = "VA" And facnormal = True And identi > 0 Then
                reportPath = RutaReportes + "\ReporteCajasGiga.rpt"
                ba = True
            Else
                If IdSistema = "VA" Then
                    'reportPath = RutaReportes + "\ReporteCajasTicketsCosmo.rpt"
                    ConfigureCrystalReports_tickets(Clv_Factura)
                    Exit Sub
                Else
                    ConfigureCrystalReports_tickets(Clv_Factura)
                    Exit Sub
                End If
            End If


            'End If

            customersByCityReport.Load(reportPath)
            'If GloImprimeTickets = False Then
            '    SetDBLogonForSubReport(connectionInfo, customersByCityReport)
            'End If
            SetDBLogonForReport(connectionInfo, customersByCityReport)


            '@Clv_Factura 
            customersByCityReport.SetParameterValue(0, Clv_Factura)
            '@Clv_Factura_Ini
            customersByCityReport.SetParameterValue(1, "0")
            '@Clv_Factura_Fin
            customersByCityReport.SetParameterValue(2, "0")
            '@Fecha_Ini
            customersByCityReport.SetParameterValue(3, "01/01/1900")
            '@Fecha_Fin
            customersByCityReport.SetParameterValue(4, "01/01/1900")
            '@op
            customersByCityReport.SetParameterValue(5, "0")

            If ba = False Then
                customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & GloEmpresa & "'"
                customersByCityReport.DataDefinition.FormulaFields("DireccionEmpresa").Text = "'" & GloDireccionEmpresa & "'"
                customersByCityReport.DataDefinition.FormulaFields("Colonia_CpEmpresa").Text = "'" & GloColonia_CpEmpresa & "'"
                customersByCityReport.DataDefinition.FormulaFields("CiudadEmpresa").Text = "'" & GloCiudadEmpresa & "'"
                customersByCityReport.DataDefinition.FormulaFields("RfcEmpresa").Text = "'" & GloRfcEmpresa & "'"
                customersByCityReport.DataDefinition.FormulaFields("TelefonoEmpresa").Text = "'" & GloTelefonoEmpresa & "'"
                If eActTickets = True Then
                    customersByCityReport.DataDefinition.FormulaFields("Mensaje").Text = "'" & eMsjTickets & "'"
                End If
            End If

            'If facticket = 1 Then
            '    customersByCityReport.PrintOptions.PrinterName = "EPSON TM-U220 Receipt"
            If (IdSistema = "TO" Or IdSistema = "SA" Or IdSistema = "AG" Or IdSistema = "VA") And facnormal = True And identi > 0 Then

                customersByCityReport.PrintOptions.PrinterName = impresorafiscal
            Else
                customersByCityReport.PrintOptions.PrinterName = LocImpresoraTickets
            End If

            If IdSistema = "AG" And facnormal = True And identi > 0 Then
                eCont = 1
                eRes = 0
                Do
                    customersByCityReport.PrintToPrinter(1, True, 1, 1)
                    eRes = MsgBox("La Impresi�n de la Factura " + CStr(eCont) + "/3, �Fu� Correcta?", MsgBoxStyle.YesNo, "Atenci�n")
                    '6=Yes;7=No
                    If eRes = 6 Then eCont = eCont + 1

                Loop While eCont <= 3
            Else
                If IdSistema = "SA" Then
                    MsgBox("Se va a imprimir una Factura Fiscal. �Est� lista la impresora?", MsgBoxStyle.OkOnly)
                    customersByCityReport.PrintToPrinter(1, True, 1, 1)
                    MsgBox("Se va a imprimir la copia de la Factura Fiscal anterior. �Est� lista la impresora?", MsgBoxStyle.OkOnly)
                    customersByCityReport.PrintToPrinter(1, True, 1, 1)
                Else
                    customersByCityReport.PrintToPrinter(1, True, 1, 1)
                End If

            End If

            'CrystalReportViewer1.ReportSource = customersByCityReport

            'If GloOpFacturas = 3 Then
            'CrystalReportViewer1.ShowExportButton = False
            'CrystalReportViewer1.ShowPrintButton = False
            'CrystalReportViewer1.ShowRefreshButton = False
            'End If
            'SetDBLogonForReport2(connectionInfo)
            customersByCityReport = Nothing
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub



    Private Sub ConfigureCrystalReports_tickets(ByVal Clv_Factura As Long)
        Try
            Dim ba As Boolean = False
            Select Case IdSistema
                Case "LO"
                    customersByCityReport = New ReporteCajasTickets_2Log

            End Select


            Dim connectionInfo As New ConnectionInfo
            '"Data Source=SERVER-2003\SQLEXPRESS;Initial Catalog=Newsoftv;Persist Security Info"& _ 
            '    "=True;User ID=DeSistema;Password=1975huli")
            connectionInfo.ServerName = GloServerName
            connectionInfo.DatabaseName = GloDatabaseName
            connectionInfo.UserID = GloUserID
            connectionInfo.Password = GloPassword

            Dim reportPath As String = Nothing

            '        If GloImprimeTickets = False Then
            'reportPath = Application.StartupPath + "\Reportes\" + "ReporteCajas.rpt"
            'Else
            'Dim CON As New SqlConnection(MiConexion)
            'CON.Open()
            'Me.BusFacFiscalTableAdapter.Connection = CON
            'Me.BusFacFiscalTableAdapter.Fill(Me.NewsoftvDataSet2.BusFacFiscal, Clv_Factura, identi)
            'CON.Close()
            'CON.Dispose()

            eActTickets = False
            Dim CON2 As New SqlConnection(MiConexion)
            CON2.Open()
            Me.DameGeneralMsjTicketsTableAdapter.Connection = CON2
            Me.DameGeneralMsjTicketsTableAdapter.Fill(Me.EricDataSet2.DameGeneralMsjTickets, eMsjTickets, eActTickets)
            CON2.Close()
            CON2.Dispose()

            'If IdSistema = "VA" Then
            '    'reportPath = RutaReportes + "\ReporteCajasTicketsCosmo.rpt"
            '    reportPath = RutaReportes + "\ReporteCajasTickets_2.rpt"
            'Else
            '    reportPath = RutaReportes + "\ReporteCajasTickets_2.rpt"
            'End If

            'End If

            'customersByCityReport.Load(reportPath)
            'If GloImprimeTickets = False Then
            '    SetDBLogonForSubReport(connectionInfo, customersByCityReport)
            'End If
            SetDBLogonForReport(connectionInfo, customersByCityReport)


            '@Clv_Factura 
            customersByCityReport.SetParameterValue(0, Clv_Factura)
            ''@Clv_Factura_Ini
            'customersByCityReport.SetParameterValue(1, "0")
            ''@Clv_Factura_Fin
            'customersByCityReport.SetParameterValue(2, "0")
            ''@Fecha_Ini
            'customersByCityReport.SetParameterValue(3, "01/01/1900")
            ''@Fecha_Fin
            'customersByCityReport.SetParameterValue(4, "01/01/1900")
            ''@op
            'customersByCityReport.SetParameterValue(5, "0")

            'If ba = False Then
            '    customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & GloEmpresa & "'"
            '    customersByCityReport.DataDefinition.FormulaFields("DireccionEmpresa").Text = "'" & GloDireccionEmpresa & "'"
            '    customersByCityReport.DataDefinition.FormulaFields("Colonia_CpEmpresa").Text = "'" & GloColonia_CpEmpresa & "'"
            '    customersByCityReport.DataDefinition.FormulaFields("CiudadEmpresa").Text = "'" & GloCiudadEmpresa & "'"
            '    customersByCityReport.DataDefinition.FormulaFields("RfcEmpresa").Text = "'" & GloRfcEmpresa & "'"
            '    customersByCityReport.DataDefinition.FormulaFields("TelefonoEmpresa").Text = "'" & GloTelefonoEmpresa & "'"
            '    If eActTickets = True Then
            '        customersByCityReport.DataDefinition.FormulaFields("Mensaje").Text = "'" & eMsjTickets & "'"
            '    End If
            'End If

            'If facticket = 1 Then
            '    customersByCityReport.PrintOptions.PrinterName = "EPSON TM-U220 Receipt"

            customersByCityReport.PrintOptions.PrinterName = LocImpresoraTickets


            customersByCityReport.PrintToPrinter(1, True, 1, 1)



            'CrystalReportViewer1.ReportSource = customersByCityReport

            'If GloOpFacturas = 3 Then
            'CrystalReportViewer1.ShowExportButton = False
            'CrystalReportViewer1.ShowPrintButton = False
            'CrystalReportViewer1.ShowRefreshButton = False
            'End If
            'SetDBLogonForReport2(connectionInfo)
            customersByCityReport.Dispose()
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub borracambiosdeposito(ByVal clv_session As Long, ByVal contrato As Long)
        Dim con As New SqlConnection(MiConexion)
        Dim cmd As New SqlClient.SqlCommand
        If locbndborracabiosdep = True Then
            locbndborracabiosdep = False
            cmd = New SqlClient.SqlCommand
            con.Open()
            With cmd
                .Connection = con
                .CommandText = "Deshacerdepositocancela"
                .CommandType = CommandType.StoredProcedure
                .CommandTimeout = 0
                '@clv_session bigint,@contrato bigint
                Dim prm As New SqlParameter("@clv_session", SqlDbType.BigInt)
                Dim prm1 As New SqlParameter("@contrato", SqlDbType.BigInt)

                prm.Direction = ParameterDirection.Input
                prm1.Direction = ParameterDirection.Input

                prm.Value = CLng(clv_session)
                prm1.Value = CLng(contrato)

                .Parameters.Add(prm)
                .Parameters.Add(prm1)
                Dim i As Integer = cmd.ExecuteNonQuery()
            End With
            con.Close()
            con.Dispose()
        End If
    End Sub
    Private Sub BUSCACLIENTES(ByVal OP As Integer)
        Dim clv_Session1 As Long = 0
        Dim parcial As Integer
        Dim Liperiodo As String
        Try

            'If locbndborracabiosdep = True Then
            '    'locbndborracabiosdep = False
            '    borracambiosdeposito(locclvsessionpardep, Loccontratopardep)
            'End If
            'locbndborracabiosdep = False
            SiPagos = 0
            Bnd = 0
            CuantasTv = 0
            'gloClv_Session = 0
            'GloContrato = 0
            Me.LabelSubTotal.Text = 0
            Me.LabelIva.Text = 0
            Me.LabelTotal.Text = 0
            Me.LblCredito_Apagar.Text = 0
            Me.LblImporte_Total.Text = 0
            Me.LabelGRan_Total.Text = 0
            Me.LabelSaldoAnterior.Text = 0
            Me.TextImporte_Adic.Text = 0
            Me.PanelTel.Visible = True
            Me.PanelNrm.Visible = False
            Me.Panel5.Visible = False
            Me.CMBPanel6.Visible = False
            BndError = 0
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            If IsNumeric(Me.Clv_Session.Text) = True Then
                'Dim CON As New SqlConnection(MiConexion)
                'CON.Open()

                Me.BorraClv_SessionTableAdapter.Connection = CON
                Me.BorraClv_SessionTableAdapter.Fill(Me.NewsoftvDataSet.BorraClv_Session, New System.Nullable(Of Long)(CType(Me.Clv_Session.Text, Long)))
                'CON.Close()
            End If
            If IsNumeric(GloTelClv_Session) = True Then
                'Dim CON As New SqlConnection(MiConexion)
                'CON.Open()
                Me.BorraClv_SessionTableAdapter.Connection = CON
                Me.BorraClv_SessionTableAdapter.Fill(Me.NewsoftvDataSet.BorraClv_Session, New System.Nullable(Of Long)(CType(GloTelClv_Session, Long)))
                'CON.Close()
            End If
            If IsNumeric(gloClv_Session) = True Then
                'Dim CON As New SqlConnection(MiConexion)
                'CON.Open()

                Me.BorraClv_SessionTableAdapter.Connection = CON
                Me.BorraClv_SessionTableAdapter.Fill(Me.NewsoftvDataSet.BorraClv_Session, New System.Nullable(Of Long)(CType(gloClv_Session, Long)))
                'CON.Close()
            End If
            If IsNumeric(clv_Session1) = True Then
                'Dim CON As New SqlConnection(MiConexion)
                'CON.Open()

                Me.BorraClv_SessionTableAdapter.Connection = CON
                Me.BorraClv_SessionTableAdapter.Fill(Me.NewsoftvDataSet.BorraClv_Session, New System.Nullable(Of Long)(CType(clv_Session1, Long)))
                'CON.Close()
            End If

            gloClv_Session = 0
            clv_Session1 = 0

            If IsNumeric(GloContrato) = True Then
                DAmeClv_Session()
                TelMuestraDetalleCargos(GloContrato)

                'GloContrato = Me.ContratoTextBox.Text
                'Glocontratosel = Me.ContratoTextBox.Text
                'Dim CON2 As New SqlConnection(MiConexion)
                'CON2.Open()
                Dim comando As SqlClient.SqlCommand

                Me.BUSCLIPORCONTRATO_FACTableAdapter.Connection = CON
                Me.BUSCLIPORCONTRATO_FACTableAdapter.Fill(Me.NewsoftvDataSet.BUSCLIPORCONTRATO_FAC, GloContrato, 0)
                Me.BuscaBloqueadoTableAdapter.Connection = CON
                Me.BuscaBloqueadoTableAdapter.Fill(Me.NewsoftvDataSet2.BuscaBloqueado, GloContrato, num, num2)
                'Me.Dime_Si_DatosFiscalesTableAdapter.Connection = CON
                'Me.Dime_Si_DatosFiscalesTableAdapter.Fill(Me.NewsoftvDataSet2.Dime_Si_DatosFiscales, GloContrato, parcial)
                comando = New SqlClient.SqlCommand
                With comando
                    .Connection = CON
                    .CommandText = "Dime_Si_DatosFiscales "
                    .CommandType = CommandType.StoredProcedure
                    .CommandTimeout = 0
                    ' Create a SqlParameter for each parameter in the stored procedure.
                    Dim prm As New SqlParameter("@Contrato", SqlDbType.BigInt)
                    Dim prm1 As New SqlParameter("@cont", SqlDbType.Int)
                    Dim prm2 As New SqlParameter("@Periodo", SqlDbType.VarChar, 50)
                    Dim prm3 As New SqlParameter("@al_corriente", SqlDbType.VarChar, 100)
                    prm.Direction = ParameterDirection.Input
                    prm1.Direction = ParameterDirection.Output
                    prm2.Direction = ParameterDirection.Output
                    prm3.Direction = ParameterDirection.Output
                    prm.Value = GloContrato
                    prm1.Value = 0
                    prm2.Value = 0
                    prm3.Value = ""
                    .Parameters.Add(prm)
                    .Parameters.Add(prm1)
                    .Parameters.Add(prm2)
                    'If IdSistema = "VA" Then
                    .Parameters.Add(prm3)

                    Dim i As Integer = comando.ExecuteNonQuery()
                    parcial = prm1.Value
                    Liperiodo = prm2.Value

                    Me.REDLabel26.Text = prm3.Value


                End With

                If parcial > 0 Then
                    Me.REDLabel25.Visible = True
                    Me.REDLabel25.Text = " EL CLIENTE CUENTA CON DATOS FISCALES "
                Else
                    Me.REDLabel25.Visible = False
                End If
                If Liperiodo <> "" Then
                    Me.Label25.Text = Liperiodo
                Else
                    Me.Label25.Text = ""
                End If
                If IdSistema = "VA" And Me.REDLabel26.Text.Trim <> "SN" Then
                    Me.REDLabel26.Visible = True
                ElseIf IdSistema <> "VA" Then
                    Me.REDLabel26.Visible = False
                End If
                'CON2.Close()
                If num = 0 Or num2 = 0 Then
                    CREAARBOL()
                Else
                    bloqueado = 1
                    clibloqueado()
                    MsgBox("El Cliente " + Me.ContratoTextBox.Text + " Ha Sido Bloqueado por lo que no se Podr� Llevar a cabo la Queja ", MsgBoxStyle.Exclamation)
                    Me.ContratoTextBox.Text = 0
                    GloContrato = 0
                    Glocontratosel = 0
                    Me.Clv_Session.Text = 0
                    BUSCACLIENTES(0)
                    Bloque(False)
                    'Glocontratosel = 0
                End If
                'SE COMENT� EL 17 DE ENERO 2009

                'If IdSistema = "SA" Then
                '    If GloTipo = "V" Then
                '        If IsDate(Fecha_Venta.Text) = True Then
                '            'Dim CON3 As New SqlConnection(MiConexion)
                '            'CON3.Open()
                '            Me.Cobra_VentasTableAdapter.Connection = CON
                '            Me.Cobra_VentasTableAdapter.Fill(Me.DataSetEdgar.Cobra_Ventas, New System.Nullable(Of Long)(CType(GloContrato, Long)), IdSistema, 0, clv_Session1, BndError, Msg, Fecha_Venta.Value, "V")
                '            'CON3.Close()
                '        End If
                '    Else
                '        'Dim CON4 As New SqlConnection(MiConexion)
                '        'CON4.Open()
                '        Me.CobraTableAdapter.Connection = CON
                '        Me.CobraTableAdapter.Fill(Me.NewsoftvDataSet.Cobra, New System.Nullable(Of Long)(CType(GloContrato, Long)), IdSistema, 0, clv_Session1, BndError, Msg)
                '        'CON4.Close()
                '    End If
                'Else
                '    'Dim CON5 As New SqlConnection(MiConexion)
                '    'CON5.Open()
                '    Me.CobraTableAdapter.Connection = CON
                '    Me.CobraTableAdapter.Fill(Me.NewsoftvDataSet.Cobra, New System.Nullable(Of Long)(CType(GloContrato, Long)), IdSistema, 0, clv_Session1, BndError, Msg)
                '    'CON5.Close()
                'End If

                'Me.Clv_Session.Text = clv_Session1
                'gloClv_Session = clv_Session1
                If IsNumeric(BndError) = False Then BndError = 1
                If BndError = 1 Then
                    Me.LABEL19.Text = Msg
                    Me.Panel5.Visible = True
                    Me.Bloque(False)
                    Me.Button2.Enabled = False
                ElseIf bloqueado <> 1 Then
                    Me.Bloque(True)
                    bloqueado = 0
                End If
                'Dim CON6 As New SqlConnection(MiConexion)
                'CON6.Open()
                Me.Dime_Si_ProcedePagoParcialTableAdapter.Connection = CON
                Me.Dime_Si_ProcedePagoParcialTableAdapter.Fill(Me.DataSetEdgar.Dime_Si_ProcedePagoParcial, New System.Nullable(Of Long)(CType(Clv_Session.Text, Long)), New System.Nullable(Of Long)(CType(GloContrato, Long)), Bnd, CuantasTv)
                'CON6.Close()
                If Bnd = 1 And CuantasTv > 0 Then
                    CMBPanel6.Visible = True
                End If

                If Loccontratopardep <> CLng(GloContrato) Then
                    Dim cmdarn As New SqlClient.SqlCommand
                    cmdarn = New SqlCommand
                    With cmdarn
                        .Connection = CON
                        .CommandText = "Dime_si_procedepagoparcialdeposito"
                        .CommandType = CommandType.StoredProcedure
                        .CommandTimeout = 0
                        '@clv_session bigint,@contrato bigint,@bnd1 int output,@bnd2 int output
                        Dim prm As New SqlParameter("@clv_session", SqlDbType.BigInt)
                        Dim prm1 As New SqlParameter("@contrato", SqlDbType.BigInt)
                        Dim prm2 As New SqlParameter("@bnd1", SqlDbType.Int)
                        Dim prm3 As New SqlParameter("@bnd2", SqlDbType.Int)
                        prm.Direction = ParameterDirection.Input
                        prm1.Direction = ParameterDirection.Input
                        prm2.Direction = ParameterDirection.Output
                        prm3.Direction = ParameterDirection.Output
                        prm.Value = CLng(Clv_Session.Text)
                        prm1.Value = CLng(GloContrato)
                        prm2.Value = 0
                        prm3.Value = 0
                        .Parameters.Add(prm)
                        .Parameters.Add(prm1)
                        .Parameters.Add(prm2)
                        .Parameters.Add(prm3)
                        Dim i As Integer = cmdarn.ExecuteNonQuery()
                        bnd1pardep1 = prm2.Value
                        bnd1pardep2 = prm3.Value
                    End With
                    locclvsessionpardep = CLng(Clv_Session.Text)
                    Loccontratopardep = CLng(GloContrato)
                    If bnd1pardep1 = 1 Then
                        bnd2pardep = True
                        locbndborracabiosdep = True
                        FrmPagosNet.Show()
                    End If
                    If bnd1pardep2 = 1 And bnd2pardep = False Then
                        bnd2pardep = True
                        locbndborracabiosdep = True
                        FrmPagosNet.Show()
                    End If
                End If
            Else
                GloContrato = 0
                'Dim CON7 As New SqlConnection(MiConexion)
                Me.BUSCLIPORCONTRATO_FACTableAdapter.Connection = CON
                Me.BUSCLIPORCONTRATO_FACTableAdapter.Fill(Me.NewsoftvDataSet.BUSCLIPORCONTRATO_FAC, 0, 0)
                Me.CobraTableAdapter.Connection = CON
                Me.CobraTableAdapter.Fill(Me.NewsoftvDataSet.Cobra, New System.Nullable(Of Long)(CType(0, Long)), "", 0, Me.Clv_Session.Text, BndError, Msg)
                'CON7.Close()
                Me.Clv_Session.Text = 0
                Me.TreeView1.Nodes.Clear()
                Me.Bloque(False)
            End If
            CON.Dispose()
            CON.Close()
            DAMETIPOSCLIENTEDAME()
            bloqueado = 0
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub

    Private Sub CREAARBOL()

        Try
            Dim I As Integer = 0
            Dim X As Integer = 0
            Dim Y As Integer = 0
            Dim epasa As Boolean = True
            ' Assumes that customerConnection is a valid SqlConnection object.
            ' Assumes that orderConnection is a valid OleDbConnection object.
            'Dim custAdapter As SqlClient.SqlDataAdapter = New SqlClient.SqlDataAdapter( _
            '  "SELECT * FROM dbo.Customers", customerConnection)''

            'Dim customerOrders As DataSet = New DataSet()
            'custAdapter.Fill(customerOrders, "Customers")
            ' 
            'Dim pRow, cRow As DataRow
            'For Each pRow In customerOrders.Tables("Customers").Rows
            ' msgbox(pRow("CustomerID").ToString())
            'Next
            Dim CON15 As New SqlConnection(MiConexion)
            CON15.Open()

            If IsNumeric(GloContrato) = True Then
                Me.DameSerDELCliFACTableAdapter.Connection = CON15
                Me.DameSerDELCliFACTableAdapter.Fill(Me.NewsoftvDataSet.DameSerDELCliFAC, New System.Nullable(Of Long)(CType(GloContrato, Long)))
            Else
                Me.DameSerDELCliFACTableAdapter.Connection = CON15
                Me.DameSerDELCliFACTableAdapter.Fill(Me.NewsoftvDataSet.DameSerDELCliFAC, New System.Nullable(Of Long)(CType(0, Long)))
            End If
            'CON15.Dispose()
            'CON15.Close()
            Dim pasa As Boolean = False
            Dim Net As Boolean = False
            Dim dig As Boolean = False
            Dim jNet As Integer = -1
            Dim PasaJNet As Boolean = False
            Dim jDig As Integer = -1
            Dim FilaRow As DataRow
            'Me.TextBox1.Text = ""
            Me.TreeView1.Nodes.Clear()
            For Each FilaRow In Me.NewsoftvDataSet.DameSerDELCliFAC.Rows

                'MsgBox(Trim(FilaRow(1).ToString()) & " " & Trim(FilaRow(0).ToString()))
                X = 0
                'If Len(Trim(Me.TextBox1.Text)) = 0 Then
                'Me.TextBox1.Text = Trim(FilaRow("Servicio").ToString())
                'Else
                'Me.TextBox1.Text = Me.TextBox1.Text & " , " & Trim(FilaRow("Servicio").ToString())
                'End If
                'MsgBox(Mid(FilaRow("Servicio").ToString(), 1, 19))
                If Mid(FilaRow("Servicio").ToString(), 1, 3) = "---" Then
                    Me.TreeView1.Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = System.Drawing.Color.FromArgb(ColorBwr)
                    pasa = True
                    Net = False
                    dig = False
                ElseIf Mid(FilaRow("Servicio").ToString(), 1, 15) = "Servicio Basico" Then
                    Me.TreeView1.Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = System.Drawing.Color.FromArgb(ColorBwr)
                    pasa = True
                ElseIf Mid(FilaRow("Servicio").ToString(), 1, 31) = "Servicios de Televisi�n Digital" Then
                    Me.TreeView1.Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = System.Drawing.Color.FromArgb(ColorBwr)
                    pasa = True
                ElseIf Mid(FilaRow("Servicio").ToString(), 1, 21) = "Servicios de Internet" Then
                    Me.TreeView1.Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = System.Drawing.Color.FromArgb(ColorBwr)

                    pasa = True
                ElseIf Mid(FilaRow("Servicio").ToString(), 1, 22) = "Servicios de Tel�fonia" Then
                    Me.TreeView1.Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = System.Drawing.Color.FromArgb(ColorBwr)

                    pasa = True
                Else
                    If Mid(FilaRow("Servicio").ToString(), 1, 14) = "Mac Cablemodem" Then
                        Me.TreeView1.Nodes(I - 1).Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = Color.Black
                        jNet = jNet + 1
                        pasa = False
                        Net = True
                    ElseIf Mid(FilaRow("Servicio").ToString(), 1, 15) = "Aparato Digital" Then
                        Me.TreeView1.Nodes(I - 1).Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = Color.Black
                        jDig = jDig + 1
                        pasa = False
                        dig = True
                    Else
                        If Net = True Then
                            Me.TreeView1.Nodes(I - 1).Nodes(jNet).Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = Color.Black
                        ElseIf dig = True Then
                            Me.TreeView1.Nodes(I - 1).Nodes(jDig).Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = Color.Black
                        Else
                            If epasa = True Then
                                Me.TreeView1.Nodes(I - 1).Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = Color.Black
                                pasa = False
                                epasa = False
                            Else
                                Me.TreeView1.Nodes(I - 1).Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = Color.Black
                                epasa = False
                                pasa = False
                            End If

                        End If
                    End If
                End If
                If pasa = True Then I = I + 1
            Next
            CON15.Dispose()
            CON15.Close()

            'Me.TreeView1.Nodes(0).ExpandAll()
            For Y = 0 To (I - 1)
                Me.TreeView1.Nodes(Y).ExpandAll()
            Next
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub

    Private Sub ContratoTextBox_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles ContratoTextBox.KeyDown
        Me.Clv_Session.Text = 0
        GloContrato = 0
        BUSCACLIENTES(0)
        If e.KeyCode = Keys.F10 Then
            BotonGrabar()
        End If
        If e.KeyValue = Keys.Enter Then
            If bloqueado <> 1 Then
                If (Me.ContratoTextBox.Text) = "" Then
                    LiContrato = 0
                    Me.Label25.Text = ""
                    Me.REDLabel26.Visible = False
                ElseIf IsNumeric(Me.ContratoTextBox.Text) = True Then
                    LiContrato = Me.ContratoTextBox.Text
                    GloContrato = CLng(Me.ContratoTextBox.Text)
                    es_recurrente = False
                    BndSupervisorFac = False
                    Checa_es_recurrente()
                End If
                BUSCACLIENTES(0)
                Me.Bloque(True)
                'If BndSupervisorFac = True Then '--->Checar si es cargo Recurrente
                '    Me.Bloque(False)
                '    BndSupervisorFac = False
                '    es_recurrente = False
                '    bndGraboClienteRecurrente = True
                '    'BUSCACLIENTES(0)
                'End If

            End If
        End If
    End Sub

    Private Sub DAmeClv_Session()
        Dim CON2 As New SqlConnection(MiConexion)
        If IsNumeric(GloTelClv_Session) = True Then
            If GloTelClv_Session > 0 Then
                CON2.Open()
                Me.BorraClv_SessionTableAdapter.Connection = CON2
                Me.BorraClv_SessionTableAdapter.Fill(Me.NewsoftvDataSet.BorraClv_Session, New System.Nullable(Of Long)(CType(GloTelClv_Session, Long)))
                CON2.Dispose()
                CON2.Close()
            End If
        End If

        GloTelClv_Session = 0
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Dim comando As SqlClient.SqlCommand
        comando = New SqlClient.SqlCommand
        With comando
            .Connection = CON
            .CommandText = "DameClv_Session "
            .CommandType = CommandType.StoredProcedure
            .CommandTimeout = 0
            Dim prm As New SqlParameter("@Clv_Session", SqlDbType.BigInt)
            prm.Direction = ParameterDirection.Output
            prm.Value = 0
            .Parameters.Add(prm)
            Dim i As Integer = comando.ExecuteNonQuery()
            GloTelClv_Session = prm.Value
            Me.Clv_SessionTel.Text = GloTelClv_Session
        End With
        CON.Close()
    End Sub




    Private Sub FrmFacturaci�n_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        Dim valida As Integer = 0
        'Dim MSG As String
        Dim bndtodos As Integer = 0
        'Dim coneLidia As New SqlClient.SqlConnection(MiConexion)
        'Dim Cmd As New SqlClient.SqlCommand
        Try
           
            If GloTiene_Cancelaciones = True Or GloComproEquipo = True Or GloCEXTTE = True Or GloAbonoACuenta = True Then
                GloTiene_Cancelaciones = False
                GloComproEquipo = False
                GloCEXTTE = False
                GloAbonoACuenta = False

                Dim CON20 As New SqlConnection(MiConexion)
                CON20.Open()
                Me.DameDetalleTableAdapter.Connection = CON20
                Me.DameDetalleTableAdapter.Fill(Me.NewsoftvDataSet.DameDetalle, GloTelClv_Session, 0)

                Me.SumaDetalleTableAdapter.Connection = CON20
                Me.SumaDetalleTableAdapter.Fill(Me.NewsoftvDataSet.SumaDetalle, GloTelClv_Session, False, 0)
                CON20.Dispose()
                CON20.Close()
            End If




            If bnd2pardep1 = True Then
                bnd2pardep1 = False
                Dim CON20 As New SqlConnection(MiConexion)
                CON20.Open()
                Me.DameDetalleTableAdapter.Connection = CON20
                Me.DameDetalleTableAdapter.Fill(Me.NewsoftvDataSet.DameDetalle, GloTelClv_Session, 0)
                '  , Loccontratopardep
                'GUARDE 
                Me.SumaDetalleTableAdapter.Connection = CON20
                Me.SumaDetalleTableAdapter.Fill(Me.NewsoftvDataSet.SumaDetalle, GloTelClv_Session, False, 0)
                CON20.Dispose()
                CON20.Close()
            End If
            If Glocontratosel > 0 Then
                Me.ContratoTextBox.Text = 0
                Me.ContratoTextBox.Text = Glocontratosel
                GloContrato = Glocontratosel
                Glocontratosel = 0
                Me.Clv_Session.Text = 0
                If locband_pant <> 8 Then
                    Me.BUSCACLIENTES(0)
                End If
            End If
            If GloBnd = True Then
                GloBnd = False
                If Glo_Apli_Pnt_Ade = True Then
                    bndtodos = 1
                    Glo_Apli_Pnt_Ade = False
                End If
                Dim CON As New SqlConnection(MiConexion)
                CON.Open()
                Me.PagosAdelantadosTableAdapter.Connection = CON
                Me.PagosAdelantadosTableAdapter.Fill(Me.NewsoftvDataSet.PagosAdelantados, New System.Nullable(Of Long)(CType(gloClv_Session, Long)), New System.Nullable(Of Long)(CType(gloClv_Servicio, Long)), New System.Nullable(Of Long)(CType(gloClv_llave, Long)), New System.Nullable(Of Long)(CType(gloClv_UnicaNet, Long)), New System.Nullable(Of Long)(CType(gloClave, Long)), IdSistema, New System.Nullable(Of Integer)(CType(GloAdelantados, Integer)), 0, Me.CLV_TIPOCLIENTELabel1.Text, bndtodos, Me.ContratoTextBox.Text, BndError, Msg)
                CON.Dispose()
                CON.Close()
                Me.Clv_Session.Text = 0
                Me.Clv_Session.Text = gloClv_Session
                If IsNumeric(BndError) = False Then BndError = 1
                If BndError = 1 Then
                    Me.LABEL19.Text = Msg
                    Me.Panel5.Visible = True
                    Me.Bloque(False)
                ElseIf BndError = 2 Then
                    MsgBox(Msg)
                Else
                    Me.Bloque(True)
                End If
            End If
            If GloBndExt = True Then
                GloBndExt = False
                If GloClv_Txt = "CEXTV" Then
                    gloClv_Session = Me.Clv_Session.Text
                    Dim Error_1 As Long = 0
                    Dim CON8 As New SqlConnection(MiConexion)
                    CON8.Open()
                    Me.DimesiahiConexTableAdapter.Connection = CON8
                    Me.DimesiahiConexTableAdapter.Fill(Me.DataSetEdgar.DimesiahiConex, New System.Nullable(Of Long)(CType(GloTelClv_Session, Long)), Error_1)
                    If Error_1 = 0 Then
                        Me.AgregarServicioAdicionalesTableAdapter.Connection = CON8
                        Me.AgregarServicioAdicionalesTableAdapter.Fill(Me.NewsoftvDataSet.AgregarServicioAdicionales, New System.Nullable(Of Long)(CType(GloTelClv_Session, Long)), GloClv_Txt, New System.Nullable(Of Long)(CType(Me.ContratoTextBox.Text, Long)), New System.Nullable(Of Long)(CType(1, Long)), IdSistema, New System.Nullable(Of Integer)(CType(0, Integer)), New System.Nullable(Of Integer)(CType(GloExt, Integer)), 0, Me.CLV_TIPOCLIENTELabel1.Text, BndError, Msg)
                        bitsist(GloUsuario, LiContrato, GloSistema, Me.Name, "Agregar Servicios", "Se Agrego un Servicio Adicional", "Servicio Agregado: " + CStr(GloClv_Txt) + " Tv Adicionales: " + CStr(GloExt), LocClv_Ciudad)
                    Else
                        MsgBox("Primero cobre la contrataci�n tvs. adicional que ya esta en la lista")
                    End If
                    CON8.Dispose()
                    CON8.Close()
                    Me.Clv_Session.Text = 0
                    Me.Clv_Session.Text = GloTelClv_Session
                Else
                    'Borramos los Totales de los Estados de Cuenta
                    Me.LabelSubTotal.Text = 0
                    Me.LabelIva.Text = 0
                    Me.LabelTotal.Text = 0
                    Me.LblCredito_Apagar.Text = 0
                    Me.LblImporte_Total.Text = 0
                    Me.LabelGRan_Total.Text = 0
                    Me.LabelSaldoAnterior.Text = 0
                    Me.TextImporte_Adic.Text = 0

                    gloClv_Session = GloTelClv_Session
                    Dim CON9 As New SqlConnection(MiConexion)
                    CON9.Open()
                    If eBndPPE = True Then
                        eBndPPE = False
                        Me.AgregarServicioAdicionales_PPETableAdapter1.Connection = CON9
                        Me.AgregarServicioAdicionales_PPETableAdapter1.Fill(Me.DataSetEdgar.AgregarServicioAdicionales_PPE, GloTelClv_Session, eClv_Progra, eClv_Txt)
                    Else
                        Me.AgregarServicioAdicionalesTableAdapter.Connection = CON9
                        Me.AgregarServicioAdicionalesTableAdapter.Fill(Me.NewsoftvDataSet.AgregarServicioAdicionales, New System.Nullable(Of Long)(CType(GloTelClv_Session, Long)), GloClv_Txt, New System.Nullable(Of Long)(CType(Me.ContratoTextBox.Text, Long)), New System.Nullable(Of Long)(CType(0, Long)), IdSistema, New System.Nullable(Of Integer)(CType(1, Integer)), New System.Nullable(Of Integer)(CType(0, Integer)), 0, Me.CLV_TIPOCLIENTELabel1.Text, BndError, Msg)
                    End If
                    CON9.Dispose()
                    CON9.Close()

                    Me.Clv_Session.Text = 0
                    Me.Clv_Session.Text = GloTelClv_Session
                End If
                If IsNumeric(BndError) = False Then BndError = 1
                If BndError = 1 Then
                    Me.LABEL19.Text = Msg
                    Me.Panel5.Visible = True
                    Me.Bloque(False)
                ElseIf BndError = 2 Then
                    MsgBox(Msg)
                Else
                    Me.Bloque(True)
                End If
            End If
            If GloBonif = 1 Then
                GloBonif = 0
                Dim CON10 As New SqlConnection(MiConexion)
                CON10.Open()
                Me.DameDetalleTableAdapter.Connection = CON10
                Me.DameDetalleTableAdapter.Fill(Me.NewsoftvDataSet.DameDetalle, GloTelClv_Session, 0)
                'GUARDE 
                Me.SumaDetalleTableAdapter.Connection = CON10
                Me.SumaDetalleTableAdapter.Fill(Me.NewsoftvDataSet.SumaDetalle, GloTelClv_Session, False, 0)
                CON10.Dispose()
                CON10.Close()
            End If
            'If bndcontt = True Then
            '    bndcontt = False
            '    ConfigureCrystalReportsContratoTomatlan("TO")
            '    valida = MsgBox("Voltee la hoja Para Continuar la Impresi�n", MsgBoxStyle.YesNo, "Pausa")
            '    If valida = 6 Then
            '        ConfigureCrystalReportsContratoTomatlan2("TO")
            '    ElseIf valida = 7 Then
            '        MsgBox("No se continuo con la Impresion", MsgBoxStyle.Information)
            '    End If
            '    Me.ContratoTextBox.Text = 0
            'End If
            Dim mensaje As String = ""
            Dim bndmensaje As Integer = 0

            If GLOSIPAGO = 1 Then 'SI YA PAGO PASA Y SI NO AL CHORIZO
                GLOSIPAGO = 0
                If IsNumeric(GLOEFECTIVO) = False Then GLOEFECTIVO = 0
                If IsNumeric(GLOCHEQUE) = False Then GLOCHEQUE = 0
                If IsNumeric(GLOCLV_BANCOCHEQUE) = False Then GLOCLV_BANCOCHEQUE = 0
                If Len(NUMEROCHEQUE) = 0 Then NUMEROCHEQUE = ""
                If IsNumeric(GLOTARJETA) = False Then GLOTARJETA = 0
                If IsNumeric(GLOCLV_BANCOTARJETA) = False Then GLOCLV_BANCOTARJETA = 0
                If Len(NUMEROTARJETA) = 0 Then NUMEROTARJETA = ""
                If Len(TARJETAAUTORIZACION) = 0 Then TARJETAAUTORIZACION = ""
                GloOpCobro = 0
                If PanelNrm.Visible = True Then
                    GloOpCobro = 1
                End If

                'CON11.Open()
                'Me.GrabaFacturasTableAdapter.Connection = CON11
                'Me.GrabaFacturasTableAdapter.Fill(Me.NewsoftvDataSet.GrabaFacturas, Me.ContratoTextBox.Text, Me.Clv_Session.Text, GloUsuario, GloSucursal, GloCaja, GloTipo, Loc_Serie, Loc_Folio, Loc_Clv_Vendedor, BndError, Msg, GloClv_Factura)
                ', , , , , 
                ''Inicia
                Dim CON As New SqlConnection(MiConexion)
                CON.Open()
                Dim comando As SqlClient.SqlCommand
                comando = New SqlClient.SqlCommand
                With comando
                    .Connection = CON
                    .CommandText = "Tel_GrabaFacturas "
                    .CommandType = CommandType.StoredProcedure
                    .CommandTimeout = 0
                    Dim prm As New SqlParameter("@Contrato", SqlDbType.BigInt)
                    prm.Direction = ParameterDirection.Input
                    prm.Value = Me.ContratoTextBox.Text
                    .Parameters.Add(prm)

                    Dim prm1 As New SqlParameter("@Clv_Session", SqlDbType.BigInt)
                    prm1.Direction = ParameterDirection.Input
                    If GloOpCobro = 1 Then
                        prm1.Value = Me.Clv_SessionTel.Text
                        '--MsgBox(GloTelClv_Session)
                    Else
                        prm1.Value = Me.Clv_Session.Text
                    End If
                    .Parameters.Add(prm1)

                    Dim prm2 As New SqlParameter("@Cajera", SqlDbType.VarChar, 11)
                    prm2.Direction = ParameterDirection.Input
                    prm2.Value = GloUsuario
                    .Parameters.Add(prm2)

                    Dim prm3 As New SqlParameter("@Sucursal", SqlDbType.Int)
                    prm3.Direction = ParameterDirection.Input
                    prm3.Value = GloSucursal
                    .Parameters.Add(prm3)

                    Dim prm4 As New SqlParameter("@Caja", SqlDbType.Int)
                    prm4.Direction = ParameterDirection.Input
                    prm4.Value = GloCaja
                    .Parameters.Add(prm4)

                    Dim prm5 As New SqlParameter("@Tipo", SqlDbType.VarChar, 1)
                    prm5.Direction = ParameterDirection.Input
                    If Me.SplitContainer1.Panel1Collapsed = False Then
                        GloTipo = "V"
                        prm5.Value = "V"
                    Else
                        GloTipo = "C"
                        prm5.Value = "C"
                    End If

                    .Parameters.Add(prm5)

                    Dim prm6 As New SqlParameter("@Serie_V", SqlDbType.VarChar, 5)
                    prm6.Direction = ParameterDirection.Input
                    If Len(Loc_Serie) = 0 Then
                        Loc_Serie = ""
                    End If
                    prm6.Value = Loc_Serie
                    .Parameters.Add(prm6)


                    Dim prm7 As New SqlParameter("@Folio_V", SqlDbType.BigInt)
                    prm7.Direction = ParameterDirection.Input
                    If IsNumeric(Loc_Folio) = False Then
                        Loc_Folio = 0
                    End If
                    prm7.Value = Loc_Folio
                    .Parameters.Add(prm7)

                    Dim prm8 As New SqlParameter("@Clv_Vendedor", SqlDbType.Int)
                    prm8.Direction = ParameterDirection.Input
                    prm8.Value = Loc_Clv_Vendedor
                    .Parameters.Add(prm8)

                    Dim prm9 As New SqlParameter("@Pago", SqlDbType.Money)
                    prm9.Direction = ParameterDirection.Input
                    prm9.Value = Me.LblImporte_Total.Text
                    .Parameters.Add(prm9)

                    Dim prm10 As New SqlParameter("@PagoRecibido", SqlDbType.Money)
                    prm10.Direction = ParameterDirection.Input
                    prm10.Value = GloPagorecibido
                    .Parameters.Add(prm10)

                    Dim prm18 As New SqlParameter("@op", SqlDbType.Int)
                    prm18.Direction = ParameterDirection.Input
                    prm18.Value = GloOpCobro
                    .Parameters.Add(prm18)


                    Dim prm11 As New SqlParameter("@BndError", SqlDbType.Int)
                    prm11.Direction = ParameterDirection.Output
                    prm11.Value = 0
                    .Parameters.Add(prm11)

                    Dim prm12 As New SqlParameter("@Msg", SqlDbType.VarChar, 250)
                    prm12.Direction = ParameterDirection.Output
                    prm12.Value = ""
                    .Parameters.Add(prm12)

                    Dim prm17 As New SqlParameter("@Clv_FacturaSalida", SqlDbType.BigInt)
                    prm17.Direction = ParameterDirection.Output
                    prm17.Value = 0
                    .Parameters.Add(prm17)

                    Dim i As Integer = comando.ExecuteNonQuery()
                    GloClv_Factura = prm17.Value
                    mensaje = prm12.Value
                    bndmensaje = prm11.Value
                End With
                CON.Close()

                'Aqui va arnold
                If bndGraboClienteRecurrente = True Then
                    bndGraboClienteRecurrente = False
                    If GloClvSupAuto <> "" And GloClvSupAuto <> Nothing Then
                        Guarda_Cobro_Rel_Supervisor_cobrosRec(GloClv_Factura, GloClvSupAuto, CLng(Me.ContratoTextBox.Text))
                    Else
                        Guarda_Cobro_Rel_Supervisor_cobrosRec(GloClv_Factura, GloUsuario, CLng(Me.ContratoTextBox.Text))
                    End If
                    GloClvSupAuto = ""
                End If


                GloPagorecibido = 0

                'Dim CON2 As New SqlConnection(MiConexion)
                If bndmensaje = 0 Then


                    CON.Open()

                    Dim comando2 As SqlClient.SqlCommand
                    comando2 = New SqlClient.SqlCommand
                    With comando2
                        .Connection = CON
                        .CommandText = "GUARDATIPOPAGO "
                        .CommandType = CommandType.StoredProcedure
                        .CommandTimeout = 0

                        Dim prm As New SqlParameter("@Clv_Factura", SqlDbType.BigInt)
                        prm.Direction = ParameterDirection.Input
                        prm.Value = GloClv_Factura
                        .Parameters.Add(prm)

                        Dim prm2 As New SqlParameter("@GLOEFECTIVO", SqlDbType.Money)
                        prm2.Direction = ParameterDirection.Input
                        prm2.Value = GLOEFECTIVO
                        .Parameters.Add(prm2)

                        Dim prm3 As New SqlParameter("@GLOCHEQUE", SqlDbType.Money)
                        prm3.Direction = ParameterDirection.Input
                        prm3.Value = GLOCHEQUE
                        .Parameters.Add(prm3)

                        Dim prm4 As New SqlParameter("@GLOCLV_BANCOCHEQUE", SqlDbType.Int)
                        prm4.Direction = ParameterDirection.Input
                        prm4.Value = GLOCLV_BANCOCHEQUE
                        .Parameters.Add(prm4)

                        Dim prm5 As New SqlParameter("@NUMEROCHEQUE", SqlDbType.VarChar)
                        prm5.Direction = ParameterDirection.Input
                        prm5.Value = NUMEROCHEQUE
                        .Parameters.Add(prm5)

                        Dim prm6 As New SqlParameter("@GLOTARJETA", SqlDbType.Money)
                        prm6.Direction = ParameterDirection.Input
                        prm6.Value = GLOTARJETA
                        .Parameters.Add(prm6)

                        Dim prm7 As New SqlParameter("@GLOCLV_BANCOTARJETA", SqlDbType.Int)
                        prm7.Direction = ParameterDirection.Input
                        prm7.Value = GLOCLV_BANCOTARJETA
                        .Parameters.Add(prm7)

                        Dim prm8 As New SqlParameter("@NUMEROTARJETA", SqlDbType.VarChar)
                        prm8.Direction = ParameterDirection.Input
                        prm8.Value = NUMEROTARJETA
                        .Parameters.Add(prm8)

                        Dim prm9 As New SqlParameter("@TARJETAAUTORIZACION", SqlDbType.VarChar)
                        prm9.Direction = ParameterDirection.Input
                        prm9.Value = TARJETAAUTORIZACION
                        .Parameters.Add(prm9)

                        Dim j As Integer = comando2.ExecuteNonQuery()

                    End With
                    CON.Close()
                    ''Inica Guarda Si el Tipo de Pago fue con una Nota de Credito
                    'Dim CON3 As New SqlConnection(MiConexion)
                    If GLOCLV_NOTA > 0 And GloClv_Factura > 0 And GLONOTA > 0 Then
                        CON.Open()

                        Dim comando3 As SqlClient.SqlCommand
                        comando3 = New SqlClient.SqlCommand
                        With comando3
                            .Connection = CON
                            .CommandText = "GUARDATIPOPAGO_Nota_Credito "
                            .CommandType = CommandType.StoredProcedure
                            .CommandTimeout = 0

                            Dim prm As New SqlParameter("@Clv_Factura", SqlDbType.BigInt)
                            prm.Direction = ParameterDirection.Input
                            prm.Value = GloClv_Factura
                            .Parameters.Add(prm)

                            Dim prm2 As New SqlParameter("@CLV_Nota", SqlDbType.BigInt)
                            prm2.Direction = ParameterDirection.Input
                            prm2.Value = GLOCLV_NOTA
                            .Parameters.Add(prm2)

                            Dim prm3 As New SqlParameter("@GLONOTA", SqlDbType.Money)
                            prm3.Direction = ParameterDirection.Input
                            prm3.Value = GLONOTA
                            .Parameters.Add(prm3)

                            Dim j As Integer = comando3.ExecuteNonQuery()

                        End With
                        CON.Close()
                    End If

                    'Fin Guarda si el tipo de Pago es con Nota de Credito


                    ''Termina
                    'Me.GUARDATIPOPAGOTableAdapter.Connection = CON11
                    'Me.GUARDATIPOPAGOTableAdapter.Fill(Me.NewsoftvDataSet1.GUARDATIPOPAGO, GloClv_Factura, GLOEFECTIVO, GLOCHEQUE, GLOCLV_BANCOCHEQUE, NUMEROCHEQUE, GLOTARJETA, GLOCLV_BANCOTARJETA, NUMEROTARJETA, TARJETAAUTORIZACION)


                    'Dim CON11 As New SqlConnection(MiConexion)
                    'CON11.Open()
                    'Me.Dime_ContratacionTableAdapter.Connection = CON11
                    'Me.Dime_ContratacionTableAdapter.Fill(Me.NewsoftvDataSet2.Dime_Contratacion, GloClv_Factura, res)

                    'If eMotivoBonificacion.Length > 0 Then
                    '    Me.GuardaMotivosBonificacionTableAdapter.Connection = CON11
                    '    Me.GuardaMotivosBonificacionTableAdapter.Fill(Me.DataSetEdgar.GuardaMotivosBonificacion, GloClv_Factura, eMotivoBonificacion)
                    'End If
                    'graba si hay bonificacion
                    'If locBndBon1 = True Then
                    '    If GloClv_Factura > 0 Then
                    '        Me.Inserta_Bonificacion_SupervisorTableAdapter.Connection = CON11
                    '        Me.Inserta_Bonificacion_SupervisorTableAdapter.Fill(Me.Procedimientos_arnoldo.Inserta_Bonificacion_Supervisor, GloClv_Factura, LocSupBon)
                    '        bitsist(GloUsuario, Me.ContratoTextBox.Text, GloSistema, Me.Name, "Se Realizo Una Bonificaci�n", "", "Factura:" + CStr(GloClv_Factura), SubCiudad)
                    '    End If
                    'End If

                    'CON11.Dispose()
                    'CON11.Close()
                    'MsgBox(Msg)
                    If LocImpresoraTickets = "" Then
                        MsgBox("No Se Ha Asignado Una Impresora de Tickets A Esta Sucursal", MsgBoxStyle.Information)
                        GLOSIPAGO = 0
                        Me.ContratoTextBox.Text = 0
                        GloContrato = 0
                        Glocontratosel = 0
                        Me.Clv_Session.Text = 0
                        BUSCACLIENTES(0)
                    Else
                        'ConfigureCrystalReports_tickets(GloClv_Factura)
                        LiTipo = 6
                        FrmImprimir.Show()
                    End If

                    Mana_ImprimirOrdenes(GloClv_Factura)
                    If GloTipo = "V" Then
                        Me.Dame_UltimoFolio()
                    End If

                    'FrmImprimir.Show()
                    If res = 1 Then
                        If LocNomImpresora_Contratos = "" Then
                            MsgBox("No se ha asignado una Impresora de Contratos a esta Sucursal", MsgBoxStyle.Information)
                            Me.ContratoTextBox.Text = 0
                            GloContrato = 0
                            Glocontratosel = 0
                            Me.Clv_Session.Text = 0
                            BUSCACLIENTES(0)
                        Else
                            If IdSistema = "TO" Then
                                '' FrmHorasInst.Show()
                                horaini = "0"
                                horafin = "0"
                                bndcontt = True
                                If bndcontt = True Then
                                    bndcontt = False
                                    ConfigureCrystalReportsContratoTomatlan("TO")
                                    valida = MsgBox("Voltee la hoja Para Continuar la Impresi�n", MsgBoxStyle.YesNo, "Pausa")
                                    If valida = 6 Then
                                        ConfigureCrystalReportsContratoTomatlan2("TO")
                                    ElseIf valida = 7 Then
                                        MsgBox("No se continuo con la Impresion", MsgBoxStyle.Information)
                                    End If
                                    Me.ContratoTextBox.Text = 0
                                    GloContrato = 0
                                    Glocontratosel = 0
                                    Me.Clv_Session.Text = 0
                                    BUSCACLIENTES(0)
                                End If
                            End If
                        End If
                    End If
                End If
                MsgBox(mensaje)
                Me.ContratoTextBox.Text = 0
                GloContrato = 0
                Glocontratosel = 0
                Me.Clv_Session.Text = 0
                BUSCACLIENTES(0)

                'Proceso para Checar el Monto de la Cajera ===========
                'coneLidia.Open()
                'With Cmd
                '    .CommandText = "Verifica_Monto"
                '    .CommandTimeout = 0
                '    .CommandType = CommandType.StoredProcedure
                '    .Connection = coneLidia
                '    Dim prm As New SqlParameter("@Cajera", SqlDbType.VarChar)
                '    Dim prm2 As New SqlParameter("@Alerta", SqlDbType.Int)
                '    prm.Direction = ParameterDirection.Input
                '    prm2.Direction = ParameterDirection.Output
                '    prm.Value = GloUsuario
                '    prm2.Value = 0
                '    .Parameters.Add(prm)
                '    .Parameters.Add(prm2)
                '    Dim i As Integer = Cmd.ExecuteNonQuery
                '    Verifica = prm2.Value
                'End With
                'If Verifica = 5 Then
                '    BndAlerta = True
                'Else
                '    BndAlerta = False
                'End If
                'Me.Enabled = True
            ElseIf GLOSIPAGO = 2 Then 'CANCELO EL PAGO
                GLOSIPAGO = 0
                Me.ContratoTextBox.Text = 0
                GloContrato = 0
                Glocontratosel = 0
                Me.Clv_Session.Text = 0
                BUSCACLIENTES(0)
                'Me.Enabled = True
            End If
            If BndSupervisorFac = True Then '--->Checar si es cargo Recurrente
                BndSupervisorFac = False
                es_recurrente = False
                'bndGraboClienteRecurrente = True
                BUSCACLIENTES(0)
            End If
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
            GLOSIPAGO = 0
            Me.ContratoTextBox.Text = 0
            GloContrato = 0
            Glocontratosel = 0
            Me.Clv_Session.Text = 0
            BUSCACLIENTES(0)
        End Try
        If eBotonGuardar = True Then
            Bloque(False)
        End If
    End Sub


    Private Sub ConfigureCrystalReportsContratoTomatlan(ByVal clv_empresa As String)
        Try
            Dim impresora As String = Nothing
            'Dim horaini As String = nothing
            'Dim horafin As String = nothing
            customersByCityReport = New ReportDocument
            Dim connectionInfo As New ConnectionInfo

            connectionInfo.ServerName = GloServerName
            connectionInfo.DatabaseName = GloDatabaseName
            connectionInfo.UserID = GloUserID
            connectionInfo.Password = GloPassword

            Dim reportPath As String = Nothing
            reportPath = RutaReportes + "\ContratoTomatlan.rpt"
            customersByCityReport.Load(reportPath)


            SetDBLogonForReport(connectionInfo, customersByCityReport)

            '@Contrato 
            customersByCityReport.SetParameterValue(0, GloContrato)
            '@clv_empresa
            customersByCityReport.SetParameterValue(1, clv_empresa)


            'horaini = InputBox("Apartir de ", "Captura Hora")
            'horafin = InputBox("Capture la hora de la Instalaci�n Final", "Captura Hora")



            'Me.Selecciona_ImpresoraTableAdapter.Fill(Me.DataSetarnoldo.Selecciona_Impresora, 1, impresora)
            'If horaini = "" Then horaini = "0"
            'If horafin = "" Then horafin = "0"

            customersByCityReport.DataDefinition.FormulaFields("horaini").Text = "'" & horaini & "'"
            customersByCityReport.DataDefinition.FormulaFields("horafin").Text = "'" & horafin & "'"


            ' customersByCityReport.PrintOptions.PrinterName = LocNomImpresora_Contratos
            customersByCityReport.PrintToPrinter(2, True, 1, 1)
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            Me.Hora_insTableAdapter.Connection = CON
            Me.Hora_insTableAdapter.Fill(Me.NewsoftvDataSet2.Hora_ins, horaini, horafin, GloContrato)
            Me.Inserta_Comentario2TableAdapter.Connection = CON
            Me.Inserta_Comentario2TableAdapter.Fill(Me.NewsoftvDataSet2.Inserta_Comentario2, GloContrato)
            CON.Dispose()
            CON.Close()
            'customersByCityReport = Nothing
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub


    Private Sub ConfigureCrystalReportsContratoTomatlan2(ByVal clv_empresa As String)
        Try
            Dim impresora As String = Nothing
            Dim horaini As String = Nothing
            Dim horafin As String = Nothing
            customersByCityReport = New ReportDocument
            Dim connectionInfo As New ConnectionInfo

            connectionInfo.ServerName = GloServerName
            connectionInfo.DatabaseName = GloDatabaseName
            connectionInfo.UserID = GloUserID
            connectionInfo.Password = GloPassword

            Dim reportPath As String = Nothing
            reportPath = RutaReportes + "\TomatlanAtras.rpt"
            customersByCityReport.Load(reportPath)


            SetDBLogonForReport(connectionInfo, customersByCityReport)

            '@Contrato 
            customersByCityReport.SetParameterValue(0, GloContrato)
            '@clv_empresa
            customersByCityReport.SetParameterValue(1, clv_empresa)

            'customersByCityReport.PrintOptions.PrinterName = LocNomImpresora_Contratos
            customersByCityReport.PrintToPrinter(2, True, 1, 1)
            'customersByCityReport = Nothing
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub


    Private Sub FrmFacturaci�n_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing

        If IsNumeric(Me.Clv_Session.Text) = True Then
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            Me.BorraClv_SessionTableAdapter.Connection = CON
            Me.BorraClv_SessionTableAdapter.Fill(Me.NewsoftvDataSet.BorraClv_Session, New System.Nullable(Of Long)(CType(Me.Clv_Session.Text, Long)))
            CON.Dispose()
            CON.Close()
        End If
        If IsNumeric(GloTelClv_Session) = True Then
            Dim CON3 As New SqlConnection(MiConexion)
            CON3.Open()
            Me.BorraClv_SessionTableAdapter.Connection = CON3
            Me.BorraClv_SessionTableAdapter.Fill(Me.NewsoftvDataSet.BorraClv_Session, New System.Nullable(Of Long)(CType(GloTelClv_Session, Long)))
            CON3.Dispose()
            CON3.Close()
        End If
    End Sub



    Private Sub FrmFac_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        colorea(Me)
        Me.Label26.ForeColor = Color.Black
        Me.Label27.ForeColor = Color.Black
        Me.Label28.ForeColor = Color.Black
        Me.LabelSubTotal.ForeColor = Color.Black
        Me.LabelIva.ForeColor = Color.Black
        Me.LabelTotal.ForeColor = Color.Black
        Me.LblCredito_Apagar.ForeColor = Color.Black
        Me.LblImporte_Total.ForeColor = Color.Black
        Me.Label29.ForeColor = Color.Black
        Me.LabelSaldoAnterior.ForeColor = Color.Black
        If bndGraboClienteRecurrente = True Then
            bndGraboClienteRecurrente = False
        End If

        'TODO: esta l�nea de c�digo carga datos en la tabla 'DataSetEdgar.MUESTRAVENDEDORES_2' Puede moverla o quitarla seg�n sea necesario.

        'TODO: esta l�nea de c�digo carga datos en la tabla 'NewsoftvDataSet.Ultimo_SERIEYFOLIO' Puede moverla o quitarla seg�n sea necesario.
        'TODO: esta l�nea de c�digo carga datos en la tabla 'NewsoftvDataSet.MuestraVendedores' Puede moverla o quitarla seg�n sea necesario.

        'TODO: esta l�nea de c�digo carga datos en la tabla 'NewsoftvDataSet.MuestraPromotores' Puede moverla o quitarla seg�n sea necesario.

        'TODO: esta l�nea de c�digo carga datos en la tabla 'NewsoftvDataSet.DameDatosGenerales' Puede moverla o quitarla seg�n sea necesario.
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.DamedatosUsuarioTableAdapter.Connection = CON
        Me.DamedatosUsuarioTableAdapter.Fill(Me.NewsoftvDataSet.DamedatosUsuario, GloUsuario)
        Me.DAMENOMBRESUCURSALTableAdapter.Connection = CON
        Me.DAMENOMBRESUCURSALTableAdapter.Fill(Me.NewsoftvDataSet.DAMENOMBRESUCURSAL, GloSucursal)
        Me.DameDatosGeneralesTableAdapter.Connection = CON
        Me.DameDatosGeneralesTableAdapter.Fill(Me.NewsoftvDataSet.DameDatosGenerales)
        CON.Dispose()
        CON.Close()
        Me.REDLabel25.Visible = False
        Me.LblNomCaja.Text = GlonOMCaja
        Me.LblVersion.Text = My.Application.Info.Version.ToString
        If GloTipo = "V" Then
            Me.SplitContainer1.Panel1Collapsed = False
            Dim CON1 As New SqlConnection(MiConexion)
            CON1.Open()
            Me.MUESTRAVENDEDORES_2TableAdapter.Connection = CON1
            Me.MUESTRAVENDEDORES_2TableAdapter.Fill(Me.DataSetEdgar.MUESTRAVENDEDORES_2)
            CON1.Dispose()
            CON1.Close()
            Me.ComboBox1.Text = ""
            Me.ComboBox1.SelectedValue = 0
        Else
            Me.SplitContainer1.Panel1Collapsed = True
            Me.ComboBox1.TabStop = False
            Me.ComboBox2.TabStop = False
        End If
        Label22.Visible = False
        Fecha_Venta.Visible = False
        If IdSistema = "SA" Then
            Me.Button12.Visible = True
            'Me.Button9.Visible = False
            If GloTipo = "V" Then
                Label22.Visible = True
                Fecha_Venta.Visible = True
            End If
        End If
        Dim CON2 As New SqlConnection(MiConexion)
        CON2.Open()
        Me.Selecciona_Impresora_SucursalTableAdapter.Connection = CON2
        Me.Selecciona_Impresora_SucursalTableAdapter.Fill(Me.NewsoftvDataSet2.Selecciona_Impresora_Sucursal, GloSucursal, LocNomImpresora_Tarjetas, LocNomImpresora_Contratos)
        CON2.Dispose()
        CON2.Close()
        Bloque(False)
        Me.Text = "Recepci�n de Pagos"
        UspGuardaFormularios(Me.Name, Me.Text)
        UspGuardaBotonesFormularioSiste(Me, Me.Name)
        UspDesactivaBotones(Me, Me.Name)
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Glocontratosel = 0
        GloContrato = 0
        eBotonGuardar = False
        'If IdSistema = "VA" Or IdSistema = "LO" Then
        '    FrmSelCliente2.Show()
        'ElseIf IdSistema <> "VA" And IdSistema <> "LO" Then
        FrmSelCliente.Show()
        'End If
    End Sub


    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        'If locbndborracabiosdep = True Then
        '    'locbndborracabiosdep = False
        '    borracambiosdeposito(locclvsessionpardep, Loccontratopardep)
        'End If
        Me.Close()
    End Sub


    Private Sub Clv_Session_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Clv_Session.TextChanged
        If IsNumeric(Me.Clv_Session.Text) = True Then
            Me.Panel5.Visible = False
            locBndBon1 = False
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            Me.DameDetalleTableAdapter.Connection = CON
            Me.DameDetalleTableAdapter.Fill(Me.NewsoftvDataSet.DameDetalle, GloTelClv_Session, 0)
            Me.SumaDetalleTableAdapter.Connection = CON
            Me.SumaDetalleTableAdapter.Fill(Me.NewsoftvDataSet.SumaDetalle, GloTelClv_Session, False, 0)
            CON.Dispose()
            CON.Close()
            ''CREAARBOL1()
        End If
    End Sub

    Private Sub DataGridView1_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridView1.CellContentClick
        If e.ColumnIndex = 7 Then
            If SiPagos = 1 Then
                MsgBox("No se puede adelantar pagos con la promoci�n que se le esta aplicando")
                Exit Sub
            End If
            If DataGridView1.SelectedCells(7).Value = "Adelantar Pagos" Then
                If IsNumeric(DataGridView1.SelectedCells(0).Value) = True And IsNumeric(DataGridView1.SelectedCells(1).Value) = True And IsNumeric(DataGridView1.SelectedCells(2).Value) = True And IsNumeric(Me.ContratoTextBox.Text) = True Then
                    gloClv_Session = DataGridView1.SelectedCells(0).Value
                    gloClv_Servicio = DataGridView1.SelectedCells(1).Value
                    gloClv_llave = DataGridView1.SelectedCells(2).Value
                    gloClv_UnicaNet = DataGridView1.SelectedCells(3).Value
                    gloClave = DataGridView1.SelectedCells(4).Value
                    Dim ERROR_1 As Integer = 0
                    Dim MSGERROR_1 As String = Nothing
                    Dim CON As New SqlConnection(MiConexion)
                    CON.Open()
                    Me.Pregunta_Si_Puedo_AdelantarTableAdapter.Connection = CON
                    Me.Pregunta_Si_Puedo_AdelantarTableAdapter.Fill(Me.DataSetEdgar.Pregunta_Si_Puedo_Adelantar, New System.Nullable(Of Long)(CType(gloClv_Session, Long)), ERROR_1, MSGERROR_1)
                    CON.Dispose()
                    CON.Close()
                    If ERROR_1 = 0 Then
                        My.Forms.FrmPagosAdelantados.Show()
                    ElseIf ERROR_1 = 2 Then
                        MsgBox(MSGERROR_1)
                    End If
                ElseIf DataGridView1.SelectedCells(7).Value = "Ext. Adicionales" Then
                    GloClv_Txt = "CEXTV"
                    Dim FrmExt As New FrmExtecionesTv
                    FrmExt.ShowDialog()

                End If
            ElseIf DataGridView1.SelectedCells(7).Value = "Ver Detalle" Then
                gloClv_UnicaNet = DataGridView1.SelectedCells(3).Value
                FrmDetCobroDesc.Show()
            End If
        End If
    End Sub

    Private Sub clibloqueado()
        Me.Button7.Enabled = False
        'Me.Button8.Enabled = False
        Me.Button2.Enabled = False
        Me.Button6.Enabled = False
        Me.Button5.Enabled = False
        Me.NOMBRELabel1.Text = ""
        Me.CALLELabel1.Text = ""
        Me.NUMEROLabel1.Text = ""
        Me.COLONIALabel1.Text = ""
        Me.CIUDADLabel1.Text = ""
    End Sub

    Private Sub Bloque(ByVal bnd As Boolean)
        Me.Button5.Enabled = bnd
        If IsNumeric(Me.ContratoTextBox.Text) = True Then
            If Me.ContratoTextBox.Text > 0 Then
                Me.Button5.Enabled = True
            End If
        End If
        'Me.Button6.Enabled = bnd
        Me.Button7.Enabled = bnd
        'Me.Button8.Enabled = bnd
        Me.Button2.Enabled = bnd
        Me.Button6.Enabled = bnd

        'DATAGRIDVIEW1 ES PARA EL FAC NORMAL
        'If Me.DataGridView1.RowCount = 0 Then
        If IsNumeric(GloContrato) = False Then GloContrato = 0
        If GloContrato = 0 Or es_recurrente = True Then

            Me.Button7.Enabled = False
            Me.Button6.Enabled = False
            Me.Button2.Enabled = False
            Me.Button5.Enabled = False
            Me.Button12.Enabled = False
            Me.ButtonVentaEq.Enabled = False
            ButtonPagoAbono.Enabled = False
        Else
            Me.Button7.Enabled = True
            Me.Button6.Enabled = True
            Me.Button2.Enabled = True
            Me.Button8.Enabled = True
            Me.Button5.Enabled = True
            Me.Button12.Enabled = True
            Me.ButtonVentaEq.Enabled = True
            ButtonPagoAbono.Enabled = True
        End If
        'If es_recurrente = True And ya_entre = 0 Then
        '    'es_recurrente = False
        '    Me.Button2.Enabled = False
        '    ya_entre = 1
        'ElseIf es_recurrente = False Then
        '    Me.Button2.Enabled = True
        'End If


        'If Me.ContratoTextBox.Text.Length = 0 Then
        '    Me.ContratoTextBox.Text = 0
        'End If
        '1:
        '        Dim CON2 As New SqlConnection(MiConexion)
        '        CON2.Open()
        '        Me.DameServicioAsignadoTableAdapter.Connection = CON2
        '        Me.DameServicioAsignadoTableAdapter.Fill(Me.EricDataSet.DameServicioAsignado, GloContrato, eRes)
        '        CON2.Dispose()
        '        CON2.Close()
        '        If eRes = 1 Then
        '            eRes = 0
        '            Me.Button9.Enabled = True
        '        Else
        '            eRes = 0
        '            Me.Button9.Enabled = False
        '        End If
    End Sub


    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Me.LabelSubTotal.Text = 0
        Me.LabelIva.Text = 0
        Me.LabelTotal.Text = 0
        Me.LblCredito_Apagar.Text = 0
        Me.LblImporte_Total.Text = 0
        Me.LabelGRan_Total.Text = 0
        Me.LabelSaldoAnterior.Text = 0
        Me.TextImporte_Adic.Text = 0
        Me.PanelTel.Visible = False
        Me.PanelNrm.Visible = True
        Me.DataGridView2.Visible = True
        Me.Clv_Session.Text = GloTelClv_Session
        gloClv_Session = GloTelClv_Session
        eBotonGuardar = False
        FrmServicios.Show()
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        Me.ContratoTextBox.Text = 0
        GloContrato = 0
        Glocontratosel = 0
        Me.Clv_Session.Text = 0
        BUSCACLIENTES(0)
        Bloque(False)
    End Sub


    Private Sub BotonGrabar()
        Try
            If Me.LblImporte_Total.Text < 0 Then
                MsgBox("El Importe tiene que ser mayor a 0", MsgBoxStyle.Information)
                Exit Sub
            End If

            If IsNumeric(Me.ContratoTextBox.Text) = True And (Me.DataGridView2.RowCount > 0 Or Me.DataGridView1.RowCount > 0) Then

                If Me.ContratoTextBox.Text > 0 And (Me.DataGridView2.RowCount > 0 Or Me.DataGridView1.RowCount > 0) Then
                    If GloTipo = "V" Then
                        If Len(Trim(Me.ComboBox2.Text)) = 0 Then
                            MsgBox("Seleccione la Serie ", MsgBoxStyle.Information)
                            Exit Sub
                        End If
                        If IsNumeric(Me.ComboBox1.SelectedValue) = False And Len(Trim(Me.ComboBox1.Text)) > 0 Then
                            MsgBox("Seleccione el Vendedor", MsgBoxStyle.Information)
                            Exit Sub
                        End If
                        If IsNumeric(Me.FolioTextBox.Text) = False Then
                            MsgBox("Capture el Folio ", MsgBoxStyle.Information)
                            Exit Sub
                        End If
                        Loc_Serie = Me.ComboBox2.Text
                        Loc_Clv_Vendedor = Me.ComboBox1.SelectedValue
                        Loc_Folio = Me.FolioTextBox.Text
                    End If
                    'Dim CON3 As New SqlConnection(MiConexion)
                    'CON3.Open()
                    'Me.DAMETOTALSumaDetalleTableAdapter.Connection = CON3
                    'Me.DAMETOTALSumaDetalleTableAdapter.Fill(Me.NewsoftvDataSet1.DAMETOTALSumaDetalle, Me.Clv_Session.Text, 0, GLOIMPTOTAL)
                    'CON3.Dispose()
                    'CON3.Close()
                    If IsNumeric(Me.LblImporte_Total.Text) = False Then Me.LblImporte_Total.Text = 0
                    GLOIMPTOTAL = Me.LblImporte_Total.Text
                    GLOSIPAGO = 0
                    eBotonGuardar = True
                    FrmPago.Show()
                    'Me.Enabled = False
                Else
                    MsgBox("Seleccione un Cliente para Facturar ", MsgBoxStyle.Information)
                    eBotonGuardar = False
                End If
            End If
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show("No hay Servicios o Conceptos que Facturar")
            Me.ContratoTextBox.Text = 0
            GloContrato = 0
            Glocontratosel = 0
            Me.Clv_Session.Text = 0
            BUSCACLIENTES(0)
            eBotonGuardar = True
        End Try
    End Sub
    Private Sub TextBox1_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        BUSCACLIENTES(0)
    End Sub

    Private Sub Clv_Vendedor_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        'If IsNumeric(Me.Clv_Vendedor.SelectedValue) = True And Len(Trim(Me.Clv_Vendedor.Text)) > 0 Then
        ' Me.Ultimo_SERIEYFOLIOTableAdapter.Fill(Me.NewsoftvDataSet.Ultimo_SERIEYFOLIO, Me.Clv_Vendedor.SelectedValue)
        ' Me.ComboBox2.Text = ""
        ' Else
        ' Me.Ultimo_SERIEYFOLIOTableAdapter.Fill(Me.NewsoftvDataSet.Ultimo_SERIEYFOLIO, 0)
        'End If
    End Sub



    Private Sub ComboBox2_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If IsNumeric(Me.ComboBox2.SelectedValue) = True And Len(Trim(ComboBox2.Text)) Then
            Dame_UltimoFolio()
        Else
            Me.FolioTextBox.Text = ""
        End If
    End Sub

    Private Sub Dame_UltimoFolio()
        Try
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            Me.FolioTextBox.Text = 0
            Me.DAMEUltimo_FOLIOTableAdapter.Connection = CON
            Me.DAMEUltimo_FOLIOTableAdapter.Fill(Me.DataSetEdgar.DAMEUltimo_FOLIO, Me.ComboBox1.SelectedValue, Me.ComboBox2.Text, Me.FolioTextBox.Text)
            CON.Dispose()
            CON.Close()
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub

    Private Sub ComboBox1_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles ComboBox1.KeyDown
        If e.KeyCode = Keys.F10 Then
            BotonGrabar()
        End If
    End Sub
    Private Sub ComboBox1_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ComboBox1.TextChanged
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        If IsNumeric(Me.ComboBox1.SelectedValue) = True And Len(Trim(Me.ComboBox1.Text)) > 0 Then
            Me.Ultimo_SERIEYFOLIOTableAdapter.Connection = CON
            Me.Ultimo_SERIEYFOLIOTableAdapter.Fill(Me.DataSetEdgar.Ultimo_SERIEYFOLIO, Me.ComboBox1.SelectedValue)
            Me.ComboBox2.Text = ""
        Else
            Me.Ultimo_SERIEYFOLIOTableAdapter.Connection = CON
            Me.Ultimo_SERIEYFOLIOTableAdapter.Fill(Me.DataSetEdgar.Ultimo_SERIEYFOLIO, 0)
        End If
        CON.Dispose()
        CON.Close()
    End Sub

    Private Sub ComboBox2_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles ComboBox2.KeyDown
        If e.KeyCode = Keys.F10 Then
            BotonGrabar()
        End If
    End Sub

    Private Sub ComboBox2_SelectedIndexChanged_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox2.SelectedIndexChanged
        Me.FolioTextBox.Text = ""
        If Len(Trim(ComboBox2.Text)) Then
            Dame_UltimoFolio()
        End If
    End Sub

    Private Sub ComboBox2_TextChanged1(ByVal sender As Object, ByVal e As System.EventArgs) Handles ComboBox2.TextChanged
        Me.FolioTextBox.Text = ""
    End Sub

    Private Sub Button8_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button8.Click
        'If IsNumeric(GloContrato) = True And GloContrato > 0 Then
        If IsNumeric(Me.ContratoTextBox.Text) = True And CInt(Me.ContratoTextBox.Text) > 0 Then
            'GloOpFacturas = 3
            'eBotonGuardar = False
            'BrwFacturas_Cancelar.Show()
            FrmSeleccionaTipo.Show()
        Else
            MsgBox("Seleccione un Cliente por favor", MsgBoxStyle.Information)
        End If
    End Sub

    Private Sub Button7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button7.Click
        Try
            If DataGridView1.RowCount > 0 Then
                Dim CON As New SqlConnection(MiConexion)
                CON.Open()
                Dim LOCCLAVE As Integer = 0
                'MsgBox(DataGridView1.SelectedCells(29).Value)
                If IsNumeric(DataGridView1.SelectedCells(29).Value) = True And IsNumeric(DataGridView1.SelectedCells(0).Value) = True Then
                    If IsNumeric(DataGridView1.SelectedCells(4).Value) = False Then LOCCLAVE = 0 Else LOCCLAVE = DataGridView1.SelectedCells(4).Value
                    If (LOCCLAVE = 1 Or LOCCLAVE = 3) And DataGridView1.SelectedCells(7).Value <> "Ext. Adicionales" Then
                        If LOCCLAVE = 1 Then MsgBox("No se puede quitar la Contrataci�n", MsgBoxStyle.Information)
                        If LOCCLAVE = 2 Then MsgBox("No se puede quitar la Reconexi�n", MsgBoxStyle.Information)
                        Exit Sub
                    End If
                    '--MsgBox(DataGridView1.SelectedCells(0).Value & "," & DataGridView1.SelectedCells(29).Value & "," & IdSistema & "," & Me.CLV_TIPOCLIENTELabel1.Text)
                    Me.BORCAMDOCFAC_QUITATableAdapter.Connection = CON
                    Me.BORCAMDOCFAC_QUITATableAdapter.Fill(Me.NewsoftvDataSet2.BORCAMDOCFAC_QUITA, GloTelClv_Session)
                    Me.QUITARDELDETALLETableAdapter.Connection = CON
                    Me.QUITARDELDETALLETableAdapter.Fill(Me.NewsoftvDataSet1.QUITARDELDETALLE, DataGridView1.SelectedCells(0).Value, DataGridView1.SelectedCells(29).Value, IdSistema, Me.CLV_TIPOCLIENTELabel1.Text, BndError, Msg)
                    bitsist(GloCajera, LiContrato, GloSistema, Me.Name, "", "Se quito del detalle", "Concepto: " + CStr(DataGridView1.SelectedCells(7).Value), LocClv_Ciudad)
                    If BndError = 1 Then
                        Me.LABEL19.Text = Msg
                        Me.Panel5.Visible = True
                        Me.Bloque(False)
                    ElseIf BndError = 2 Then
                        MsgBox(Msg)
                    Else
                        Me.Bloque(True)
                    End If
                    Me.DameDetalleTableAdapter.Connection = CON
                    Me.DameDetalleTableAdapter.Fill(Me.NewsoftvDataSet.DameDetalle, GloTelClv_Session, 0)
                    Me.SumaDetalleTableAdapter.Connection = CON
                    Me.SumaDetalleTableAdapter.Fill(Me.NewsoftvDataSet.SumaDetalle, GloTelClv_Session, False, 0)
                    ''gloClv_Session = DataGridView1.SelectedCells(0).Value        
                    ''gloClave = DataGridView1.SelectedCells(4).Value
                End If
                CON.Dispose()
                CON.Close()
            Else
                MsgBox("No hay conceptos que quitar ", MsgBoxStyle.Information)
            End If
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
        eBotonGuardar = False

        'If Me.DataGridView1.RowCount = 0 Then
        ' Me.Button7.Enabled = False
        'Me.Button6.Enabled = False
        'Me.Button2.Enabled = False
        'End If
    End Sub

    Private Sub DAMETIPOSCLIENTEDAME()
        Try
            Me.CLV_TIPOCLIENTELabel1.Text = ""
            Me.DESCRIPCIONLabel1.Text = ""
            If IsNumeric(GloContrato) = True Then
                If GloContrato > 0 Then
                    Dim CON As New SqlConnection(MiConexion)
                    CON.Open()
                    Me.DAMETIPOSCLIENTESTableAdapter.Connection = CON
                    Me.DAMETIPOSCLIENTESTableAdapter.Fill(Me.DataSetEdgar.DAMETIPOSCLIENTES, Me.ContratoTextBox.Text)
                    CON.Dispose()
                    CON.Close()

                End If
            End If
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub


    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        If BndError = 1 Then
            If Me.LABEL19.BackColor = Color.Yellow Then
                Me.LABEL19.BackColor = Me.Panel5.BackColor
            Else
                Me.LABEL19.BackColor = Color.Yellow
            End If
        End If
    End Sub

    Private Sub Button6_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button6.Click
        Try
            If DataGridView1.RowCount > 0 Then
                If IsNumeric(DataGridView1.SelectedCells(29).Value) = True And IsNumeric(DataGridView1.SelectedCells(0).Value) Then
                    GloDes_Ser = DataGridView1.SelectedCells(6).Value
                    gloClv_Session = DataGridView1.SelectedCells(0).Value
                    gloClv_Detalle = DataGridView1.SelectedCells(29).Value
                    loctitulo = "Solo el Supervisor puede Bonificar"
                    locband_pant = 3
                    locBndBon1 = True
                    If IdSistema = "SA" And GloTipoUsuario = 1 Then
                        eAccesoAdmin = False
                    End If
                    FrmSupervisor.Show()
                Else
                    MsgBox("Seleccione el Concepto que deseas Bonificar ", MsgBoxStyle.Information)
                End If
            End If
            eBotonGuardar = False
        Catch ex As System.Exception
            Exit Sub
        End Try
    End Sub

    Private Sub Label13_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Label13.Click

    End Sub


    'Private Sub FillToolStripButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    Try
    '        Me.GuardaMotivosBonificacionTableAdapter.Fill(Me.DataSetEdgar.GuardaMotivosBonificacion, New System.Nullable(Of Long)(CType(Clv_FacturaToolStripTextBox.Text, Long)), DescripcionToolStripTextBox.Text)
    '    Catch ex As System.Exception
    '        System.Windows.Forms.MessageBox.Show(ex.Message)
    '    End Try

    'End Sub

    Private Sub DataGridView1_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles DataGridView1.KeyDown
        If e.KeyCode = Keys.F10 Then
            BotonGrabar()
        End If
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click

        BotonGrabar()


    End Sub

    Private Sub TreeView1_AfterSelect(ByVal sender As System.Object, ByVal e As System.Windows.Forms.TreeViewEventArgs) Handles TreeView1.AfterSelect

    End Sub

    Private Sub TreeView1_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles TreeView1.KeyDown
        If e.KeyCode = Keys.F10 Then
            BotonGrabar()
        End If
    End Sub

    Private Sub ComboBox1_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox1.SelectedIndexChanged

    End Sub

    Private Sub FolioTextBox_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles FolioTextBox.KeyDown
        If e.KeyCode = Keys.F10 Then
            BotonGrabar()
        End If
    End Sub

    Private Sub FolioTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles FolioTextBox.TextChanged

    End Sub

    Private Sub Button1_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Button1.KeyDown
        If e.KeyCode = Keys.F10 Then
            BotonGrabar()
        End If
    End Sub

    Private Sub Button8_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Button8.KeyDown
        If e.KeyCode = Keys.F10 Then
            BotonGrabar()
        End If
    End Sub

    Private Sub ConfigureCrystalReportsOrdenes(ByVal op As String, ByVal Titulo As String, ByVal Clv_Orden1 As Long, ByVal Clv_TipSer1 As Integer, ByVal GloNom_TipSer As String)
        Try
            customersByCityReport = New ReportDocument
            Dim connectionInfo As New ConnectionInfo
            Dim Op1 As String = "0", Op2 As String = "0", Op3 As String = "0", Op4 As String = "0", Op5 As String = "0"
            Dim StatusPen As String = "0", StatusEje As String = "0", StatusVis As String = "0"
            Dim Fec1Ini As String = "01/01/1900", Fec1Fin As String = "01/01/1900", Fec2Ini As String = "01/01/1900", Fec2Fin As String = "01/01/1900"
            Dim Num1 As String = 0, Num2 As String = 0
            Dim nclv_trabajo As String = "0"
            Dim nClv_colonia As String = "0"
            Dim Impresora As String = Nothing
            Dim a As Integer = 0



            '"Data Source=SERVER-2003\SQLEXPRESS;Initial Catalog=Newsoftv;Persist Security Info"& _ 
            '    "=True;User ID=DeSistema;Password=1975huli")

            'connectionInfo.ServerName = GloServerName
            'connectionInfo.DatabaseName = GloDatabaseName
            'connectionInfo.UserID = GloUserID
            'connectionInfo.Password = GloPassword

            Dim mySelectFormula As String = Titulo
            Dim OpOrdenar As String = "0"


            Dim reportPath As String = Nothing



            If IdSistema = "TO" Then
                reportPath = RutaReportes + "\ReporteFormatoOrdenesServBuenoCabStar.rpt"
            ElseIf IdSistema = "SA" Then
                reportPath = RutaReportes + "\ReporteFormatoOrdenesServBuenoTvRey.rpt"
            ElseIf IdSistema = "AG" Then
                'reportPath = RutaReportes + "\ReporteFormatoOrdenesServBueno.rpt"
                reportPath = RutaReportes + "\ReporteOrdenes.rpt"
            ElseIf IdSistema = "VA" Then
                reportPath = RutaReportes + "\ReporteFormatoOrdenesServBuenoCosmo.rpt"
            ElseIf IdSistema = "LO" Then
                reportPath = RutaReportes + "\ReporteFormatoOrdenesServBuenoLogitel.rpt"
            End If


            'customersByCityReport.Load(reportPath)
            'SetDBLogonForReport(connectionInfo, customersByCityReport)

            ''@Clv_TipSer int
            'customersByCityReport.SetParameterValue(0, 0)
            '',@op1 smallint
            'customersByCityReport.SetParameterValue(1, 1)
            '',@op2 smallint
            'customersByCityReport.SetParameterValue(2, 0)
            '',@op3 smallint
            'customersByCityReport.SetParameterValue(3, 0)
            '',@op4 smallint,
            'customersByCityReport.SetParameterValue(4, 0)
            ''@op5 smallint
            'customersByCityReport.SetParameterValue(5, 0)
            '',@StatusPen bit
            'customersByCityReport.SetParameterValue(6, 0)
            '',@StatusEje bit
            'customersByCityReport.SetParameterValue(7, 0)
            '',@StatusVis bit,
            'customersByCityReport.SetParameterValue(8, 0)
            ''@Clv_OrdenIni bigint
            'customersByCityReport.SetParameterValue(9, CLng(Clv_Orden1))
            '',@Clv_OrdenFin bigint
            'customersByCityReport.SetParameterValue(10, CLng(Clv_Orden1))
            '',@Fec1Ini Datetime
            'customersByCityReport.SetParameterValue(11, "01/01/1900")
            '',@Fec1Fin Datetime,
            'customersByCityReport.SetParameterValue(12, "01/01/1900")
            ''@Fec2Ini Datetime
            'customersByCityReport.SetParameterValue(13, "01/01/1900")
            '',@Fec2Fin Datetime
            'customersByCityReport.SetParameterValue(14, "01/01/1900")
            '',@Clv_Trabajo int
            'customersByCityReport.SetParameterValue(15, 0)
            '',@Clv_Colonia int
            'customersByCityReport.SetParameterValue(16, 0)
            '',@OpOrden int
            'customersByCityReport.SetParameterValue(17, OpOrdenar)

            Dim DS As New DataSet
            DS.Clear()
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@Clv_TipSer", SqlDbType.Int, CObj(0))
            BaseII.CreateMyParameter("@op1", SqlDbType.SmallInt, CShort(1))
            BaseII.CreateMyParameter("@op2", SqlDbType.SmallInt, CShort(0))
            BaseII.CreateMyParameter("@op3", SqlDbType.SmallInt, CShort(0))
            BaseII.CreateMyParameter("@op4", SqlDbType.SmallInt, CShort(0))
            BaseII.CreateMyParameter("@op5", SqlDbType.SmallInt, CShort(0))
            BaseII.CreateMyParameter("@StatusPen", SqlDbType.Bit, CByte(0))
            BaseII.CreateMyParameter("@StatusEje", SqlDbType.Bit, CByte(0))
            BaseII.CreateMyParameter("@StatusVis", SqlDbType.Bit, CByte(0))
            BaseII.CreateMyParameter("@Clv_OrdenIni", SqlDbType.BigInt, CLng(Clv_Orden1))
            BaseII.CreateMyParameter("@Clv_OrdenFin", SqlDbType.BigInt, CLng(Clv_Orden1))
            BaseII.CreateMyParameter("@Fec1Ini", SqlDbType.DateTime, "01/01/1900")
            BaseII.CreateMyParameter("@Fec1Fin", SqlDbType.DateTime, "01/01/1900")
            BaseII.CreateMyParameter("@Fec2Ini", SqlDbType.DateTime, "01/01/1900")
            BaseII.CreateMyParameter("@Fec2Fin", SqlDbType.DateTime, "01/01/1900")
            BaseII.CreateMyParameter("@Clv_Trabajo", SqlDbType.Int, 0)
            BaseII.CreateMyParameter("@Clv_Colonia", SqlDbType.Int, 0)
            BaseII.CreateMyParameter("@OpOrden", SqlDbType.Int, CLng(OpOrdenar))

            Dim listatablas As New List(Of String)
            'listatablas.Add("ReporteAreaTecnicaOrdSer")
            'listatablas.Add("DameDatosGenerales_2")

            'DS = BaseII.ConsultaDS("ReporteAreaTecnicaOrdSer", listatablas)

            listatablas.Add("ReporteOrdSer")
            listatablas.Add("Comentarios_DetalleOrden")
            listatablas.Add("DameDatosGenerales_2;1")
            listatablas.Add("DetOrdSer")
            listatablas.Add("Trabajos")
            listatablas.Add("ClientesConElMismoPoste")
            DS = BaseII.ConsultaDS("ReporteOrdSer", listatablas)

            customersByCityReport.Load(reportPath)
            SetDBReport(DS, customersByCityReport)




            mySelectFormula = "Orden De Servicio: "
            customersByCityReport.DataDefinition.FormulaFields("Queja").Text = "'" & mySelectFormula & "'"
            Dim CON12 As New SqlConnection(MiConexion)
            CON12.Open()
            Me.Dame_Impresora_OrdenesTableAdapter.Connection = CON12
            Me.Dame_Impresora_OrdenesTableAdapter.Fill(Me.NewsoftvDataSet2.Dame_Impresora_Ordenes, Impresora, a)
            CON12.Dispose()
            CON12.Close()

            If a = 1 Then
                MsgBox("No se tiene asignada una Impresora de Ordenes de Servicio", MsgBoxStyle.Information)
                Exit Sub
            Else
                customersByCityReport.PrintOptions.PrinterName = Impresora
                customersByCityReport.PrintToPrinter(1, True, 1, 1)
            End If
            '--SetDBLogonForReport(connectionInfo)




            customersByCityReport = Nothing
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub


    Private Sub Mana_ImprimirOrdenes(ByVal Clv_Factura As Long)
        Try
            'Eric 10Dic2008 LAS ORDENES DE SERVICIO QUE SE GENERAN DESDE FACTURACION
            'NO SE IMPRIMIRAN DE FORMA AUTOM�TICA
            If IdSistema <> "VA" Then
                Dim CON As New SqlConnection(MiConexion)
                CON.Open()
                Me.DamelasOrdenesque_GeneroFacturaTableAdapter.Connection = CON
                Me.DamelasOrdenesque_GeneroFacturaTableAdapter.Fill(Me.NewsoftvDataSet2.DamelasOrdenesque_GeneroFactura, New System.Nullable(Of Long)(CType(Clv_Factura, Long)))
                CON.Dispose()
                CON.Close()
                Dim FilaRow As DataRow
                'Me.TextBox1.Text = ""

                For Each FilaRow In Me.NewsoftvDataSet2.DamelasOrdenesque_GeneroFactura.Rows
                    If IsNumeric(FilaRow("Clv_Orden").ToString()) = True Then
                        ConfigureCrystalReportsOrdenes(0, "", FilaRow("Clv_Orden").ToString(), FilaRow("Clv_TipSer").ToString(), FilaRow("Concepto").ToString())
                    End If
                Next
            End If

        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub


    'Private Sub Button9_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    FrmServiciosPPE.Show()
    'End Sub

    Private Sub CMBPanel6_Paint(ByVal sender As System.Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles CMBPanel6.Paint

    End Sub

    Private Sub Button11_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button11.Click
        Me.CMBPanel6.Visible = False
        SiPagos = 0
    End Sub

    Private Sub Button10_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button10.Click
        Try
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            SiPagos = 1
            Me.Cobra_PagosTableAdapter.Connection = CON
            Me.Cobra_PagosTableAdapter.Fill(Me.DataSetEdgar.Cobra_Pagos, New System.Nullable(Of Long)(CType(GloContrato, Long)), New System.Nullable(Of Long)(CType(GloTelClv_Session, Long)), New System.Nullable(Of Integer)(CType(Bnd, Integer)), New System.Nullable(Of Integer)(CType(CuantasTv, Integer)))
            Me.DameDetalleTableAdapter.Connection = CON
            Me.DameDetalleTableAdapter.Fill(Me.NewsoftvDataSet.DameDetalle, GloTelClv_Session, 0)
            Me.SumaDetalleTableAdapter.Connection = CON
            Me.SumaDetalleTableAdapter.Fill(Me.NewsoftvDataSet.SumaDetalle, GloTelClv_Session, False, 0)
            CON.Dispose()
            CON.Close()
            Me.CMBPanel6.Visible = False
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub



    Private Sub Button12_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button12.Click
        Try
            Me.LabelSubTotal.Text = 0
            Me.LabelIva.Text = 0
            Me.LabelTotal.Text = 0
            Me.LblCredito_Apagar.Text = 0
            Me.LblImporte_Total.Text = 0
            Me.LabelGRan_Total.Text = 0
            Me.LabelSaldoAnterior.Text = 0
            Me.TextImporte_Adic.Text = 0
            Me.PanelTel.Visible = False
            Me.PanelNrm.Visible = True
            Me.DataGridView2.Visible = True
            Me.Clv_Session.Text = GloTelClv_Session
            gloClv_Session = GloTelClv_Session
            eBotonGuardar = False
            FrmDetalle_Conceptos_Facturar.Show()
            'Dim CON As New SqlConnection(MiConexion)
            'If IsNumeric(Me.Clv_Session.Text) = True And IsNumeric(Me.CLV_DETALLETextBox.Text) = True Then
            '    If Me.Clv_Session.Text > 0 And Me.CLV_DETALLETextBox.Text > 0 Then
            '        ePideAparato = 0
            '        Dim ERROR_1 As Integer = 0
            '        Dim msgERROR_1 As String = Nothing

            '        CON.Open()
            '        Me.CobraAdeudoTableAdapter.Connection = CON
            '        Me.CobraAdeudoTableAdapter.Fill(Me.DataSetEdgar.CobraAdeudo, New System.Nullable(Of Long)(CType(Me.Clv_Session.Text, Long)), New System.Nullable(Of Long)(CType(Me.CLV_DETALLETextBox.Text, Long)), ERROR_1, msgERROR_1, ePideAparato, eClv_Detalle)
            '        Me.DameDetalleTableAdapter.Connection = CON
            '        Me.DameDetalleTableAdapter.Fill(Me.NewsoftvDataSet.DameDetalle, Me.Clv_Session.Text, 0)
            '        Me.SumaDetalleTableAdapter.Connection = CON
            '        Me.SumaDetalleTableAdapter.Fill(Me.NewsoftvDataSet.SumaDetalle, Me.Clv_Session.Text, False, 0)
            '        CON.Dispose()
            '        CON.Close()

            '        If ERROR_1 = 1 Then
            '            Me.LABEL19.Text = msgERROR_1
            '            Me.Panel5.Visible = True
            '            Me.Bloque(False)
            '        ElseIf ERROR_1 = 2 Then
            '            MsgBox(msgERROR_1)
            '        End If
            '        'VALIDACION PARA VALLARTA. PREGUNTA SI EL CLIENTE LLEVA CONSIGO EL APARATO, SI NO, SE GENERA UNA ORDEN DE RETIRO DE APARATO O CABLEMODEM
            '        If ePideAparato = 1 Then
            '            eRes = MsgBox("�El Cliente trae con sigo el Aparato?", MsgBoxStyle.YesNo, "Atenci�n")
            '            If eRes = 6 Then
            '                Dim CON2 As New SqlConnection(MiConexion)
            '                CON2.Open()
            '                Me.EntregaAparatoTableAdapter.Connection = CON2
            '                Me.EntregaAparatoTableAdapter.Fill(Me.EricDataSet2.EntregaAparato, CLng(Me.Clv_Session.Text), eClv_Detalle)
            '                CON2.Close()
            '            End If
            '        End If

            '    End If
            'End If
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub CLV_DETALLETextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CLV_DETALLETextBox.TextChanged

    End Sub

    Private Sub Label14_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Label14.Click

    End Sub


    Private Sub LblFecha_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles LblFecha.Click

    End Sub

    Private Sub LblFecha_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles LblFecha.TextChanged
        If IsDate(LblFecha.Text) = True Then
            Me.Fecha_Venta.Value = LblFecha.Text
        End If
    End Sub

    Private Sub Fecha_Venta_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Fecha_Venta.ValueChanged
        If IsNumeric(Me.ContratoTextBox.Text) = True Then
            If Me.ContratoTextBox.Text > 0 Then
                If bloqueado <> 1 Then
                    BUSCACLIENTES(0)
                    Me.Bloque(True)
                End If
            End If
        End If
    End Sub




    Private Sub ClvSessionDataGridViewTextBoxColumn1_Disposed(ByVal sender As Object, ByVal e As System.EventArgs) Handles ClvSessionDataGridViewTextBoxColumn1.Disposed

    End Sub


    Private Sub TelMuestraDetalleCargos(ByVal Contrato As Long)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("TelMuestraDetalleCargos", conexion)
        comando.CommandType = CommandType.StoredProcedure

        Dim parametro As New SqlParameter("@Contrato", SqlDbType.BigInt)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = Contrato
        comando.Parameters.Add(parametro)

        Try
            Me.DataGridView2.Rows.Clear()
            conexion.Open()
            Dim reader As SqlDataReader = comando.ExecuteReader()
            While (reader.Read())
                Me.DataGridView2.Rows.Add(reader(0), reader(1))
            End While
            conexion.Close()
            TelMuestraDetalleCargosAPagar(Contrato)
            'If Me.DataGridView2.Rows.Count = 0 Then
            Bloque(True)
            ' Else
            'Bloque(False)
            'End If
        Catch ex As Exception
            conexion.Close()
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try
    End Sub

    Private Sub TelMuestraDetalleCargosAPagar(ByVal Contrato As Long)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("TelMuestraDetalleCargosAPagar", conexion)
        comando.CommandType = CommandType.StoredProcedure

        Dim parametro As New SqlParameter("@Contrato", SqlDbType.BigInt)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = Contrato
        comando.Parameters.Add(parametro)
        Me.Clv_Session.Text = 0
        Try
            conexion.Open()
            Dim reader As SqlDataReader = comando.ExecuteReader()
            While reader.Read()
                Me.LabelSubTotal.Text = Microsoft.VisualBasic.FormatCurrency(reader(0).ToString, 2)
                Me.LabelIva.Text = Microsoft.VisualBasic.FormatCurrency(reader(1).ToString, 2)
                Me.LabelTotal.Text = Microsoft.VisualBasic.FormatCurrency(reader(2).ToString, 2)
                Me.LblCredito_Apagar.Text = Microsoft.VisualBasic.FormatCurrency(reader(3).ToString, 2)
                Me.LblImporte_Total.Text = reader(4).ToString
                Me.LabelGRan_Total.Text = Microsoft.VisualBasic.FormatCurrency(reader(4).ToString, 2)
                Me.Clv_Session.Text = reader(5).ToString
                Me.LabelSaldoAnterior.Text = Microsoft.VisualBasic.FormatCurrency(reader(6).ToString, 2)
            End While
            conexion.Close()
        Catch ex As Exception
            conexion.Close()
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try
    End Sub


    Private Sub LabelSubTotal_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles LabelSubTotal.Click

    End Sub


    Private Sub TextImporte_Adic_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextImporte_Adic.TextChanged
        If Me.PanelNrm.Visible = True Then

            If IsNumeric(TextImporte_Adic.Text) = True Then
                Me.LabelSubTotal.Text = 0
                Me.LabelIva.Text = 0
                Me.LabelTotal.Text = 0
                Me.LblCredito_Apagar.Text = 0
                Me.LblImporte_Total.Text = 0
                Me.LabelGRan_Total.Text = 0
                Me.LabelSaldoAnterior.Text = 0
                If TextImporte_Adic.Text > 0 Then
                    Me.LabelSubTotal.Text = Microsoft.VisualBasic.FormatCurrency(Me.TextImporte_Adic.Text / 1.15, 2)
                    Me.LabelIva.Text = Microsoft.VisualBasic.FormatCurrency(Me.TextImporte_Adic.Text - Me.LabelSubTotal.Text, 2)
                    Me.LabelTotal.Text = Microsoft.VisualBasic.FormatCurrency(Me.TextImporte_Adic.Text, 2)
                    Me.LblCredito_Apagar.Text = 0
                    Me.LblImporte_Total.Text = Microsoft.VisualBasic.FormatCurrency(Me.TextImporte_Adic.Text, 2)
                    Me.LabelGRan_Total.Text = Microsoft.VisualBasic.FormatCurrency(Me.TextImporte_Adic.Text, 2)
                    Me.LabelSaldoAnterior.Text = 0
                End If
                Me.LblImporte_Total.Text = Me.TextImporte_Adic.Text
            End If
        End If
    End Sub

    Private Sub Clv_SessionTel_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Clv_SessionTel.TextChanged
        If IsNumeric(Me.Clv_SessionTel.Text) = True Then
            Me.Panel5.Visible = False
            locBndBon1 = False
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            Me.DameDetalleTableAdapter.Connection = CON
            Me.DameDetalleTableAdapter.Fill(Me.NewsoftvDataSet.DameDetalle, GloTelClv_Session, 0)
            Me.SumaDetalleTableAdapter.Connection = CON
            Me.SumaDetalleTableAdapter.Fill(Me.NewsoftvDataSet.SumaDetalle, GloTelClv_Session, False, 0)
            CON.Dispose()
            CON.Close()
            ''CREAARBOL1()
        End If
    End Sub

    Private Sub ButtonVentaEq_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonVentaEq.Click
        Me.LabelSubTotal.Text = 0
        Me.LabelIva.Text = 0
        Me.LabelTotal.Text = 0
        Me.LblCredito_Apagar.Text = 0
        Me.LblImporte_Total.Text = 0
        Me.LabelGRan_Total.Text = 0
        Me.LabelSaldoAnterior.Text = 0
        Me.TextImporte_Adic.Text = 0
        Me.PanelTel.Visible = False
        Me.PanelNrm.Visible = True
        Me.DataGridView2.Visible = True
        Me.Clv_Session.Text = GloTelClv_Session
        gloClv_Session = GloTelClv_Session
        eBotonGuardar = False
        FrmVentaDeEquipo.Show()
    End Sub

    Private Sub ContratoTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ContratoTextBox.TextChanged

    End Sub

    Private Sub ButtonPagoAbono_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonPagoAbono.Click
        Me.LabelSubTotal.Text = 0
        Me.LabelIva.Text = 0
        Me.LabelTotal.Text = 0
        Me.LblCredito_Apagar.Text = 0
        Me.LblImporte_Total.Text = 0
        Me.LabelGRan_Total.Text = 0
        Me.LabelSaldoAnterior.Text = 0
        Me.TextImporte_Adic.Text = 0
        Me.PanelTel.Visible = False
        Me.PanelNrm.Visible = True
        Me.DataGridView2.Visible = True
        Me.Clv_Session.Text = GloTelClv_Session
        gloClv_Session = GloTelClv_Session
        eBotonGuardar = False
        FrmPagoParaAbono.Show()
    End Sub
End Class
